package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by williampietro on 06/11/14.
 */
public class Chat {
    private int id;
    private int idFornecedor;
    private int idUsuario;
    private int idServico;
    private int idOrcamento;
    List<Mensagem> mensagens = new ArrayList<Mensagem>();

    public Chat(int idFornecedor, int idUsuario,int idServico, List<Mensagem> mensagens) {
        this.idFornecedor = idFornecedor;
        this.idUsuario = idUsuario;
        this.mensagens = mensagens;
        this.idServico = idServico;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(int idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdServico() {
        return idServico;
    }

    public void setIdServico(int idServico) {
        this.idServico = idServico;
    }

    public int getIdOrcamento() {
        return idOrcamento;
    }

    public void setIdOrcamento(int idOrcamento) {
        this.idOrcamento = idOrcamento;
    }

    public List<Mensagem> getMensagens() {
        return mensagens;
    }

    public void setMensagens(List<Mensagem> mensagens) {
        this.mensagens = mensagens;
    }
}
