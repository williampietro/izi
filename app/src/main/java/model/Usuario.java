package model;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by williampietro on 27/10/14.
 */
public class Usuario implements Serializable
{

    int id;

    private String  numCPF,
                    numCNPJ,
                    nome,
                    dtaNascimento,
                    apelido,
                    senha,
                    email,
                    telefone,
                    celular;
    private List<Endereco> enderecos = new ArrayList<Endereco>();

    public Usuario(){}

    public Usuario(String numCPF,
                   String numCNPJ,
                   String nome,
                   String dtaNascimento,
                   String apelido,
                   String senha,
                   String email,
                   String telefone,
                   String celular)
    {
        this.numCPF = numCPF;
        this.numCNPJ = numCNPJ;
        this.nome = nome;
        this.dtaNascimento = dtaNascimento;
        this.apelido = apelido;
        this.senha = senha;
        this.email = email;
        this.telefone = telefone;
        this.celular = celular;
    }

    public void addEndereco(Endereco end){
        enderecos.add(end);
    }

    public List<Endereco> retornaEndereco(){
        return enderecos;
    }

    public Endereco getPosition(int i){
        return enderecos.get(i);
    }
    public void removeEndereco(int i){
        enderecos.remove(i);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumCPF() {
        return numCPF;
    }

    public void setNumCPF(String numCPF) {
        this.numCPF = numCPF;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDtaNascimento() {
        return dtaNascimento;
    }

    public void setDtaNascimento(String dtaNascimento) {
        this.dtaNascimento = dtaNascimento;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void setEnderecos(List<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    public String getNumCNPJ() {
        return numCNPJ;
    }

    public void setNumCNPJ(String numCNPJ) {
        this.numCNPJ = numCNPJ;
    }
}
