package model;

import java.io.Serializable;

/**
 * Created by williampietro on 08/11/14.
 */
public class Agendamento implements Serializable {
    int id, idOrcamento, idServico,idFornecedor,idTipo;
    String dataInicio, dataFim, titulo, descricao;

    public Agendamento(){}


    @Override
    public String toString() {
        return "Agendamento{" +
                "id=" + id +
                ", idOrcamento=" + idOrcamento +
                ", idServico=" + idServico +
                ", idFornecedor=" + idFornecedor +
                ", idTipo=" + idTipo +
                ", dataInicio='" + dataInicio + '\'' +
                ", dataFim='" + dataFim + '\'' +
                ", titulo='" + titulo + '\'' +
                ", descricao='" + descricao + '\'' +
                '}';
    }

    public Agendamento(int id, int idOrcamento, int idServico, int idFornecedor,
                       int idTipo, String dataInicio, String dataFim, String titulo,
                       String descricao) {
        super();
        this.idOrcamento = idOrcamento;
        this.idServico = idServico;
        this.idFornecedor = idFornecedor;
        this.idTipo = idTipo;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.titulo = titulo;
        this.descricao = descricao;
        this.id = id;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getIdOrcamento() {
        return idOrcamento;
    }
    public void setIdOrcamento(int idOrcamento) {
        this.idOrcamento = idOrcamento;
    }
    public int getIdSolicitacao() {
        return idServico;
    }
    public void setIdSolicitacao(int idSolicitacao) {
        this.idServico = idSolicitacao;
    }
    public int getIdFornecedor() {
        return idFornecedor;
    }
    public void setIdFornecedor(int idFornecedor) {
        this.idFornecedor = idFornecedor;
    }
    public int getIdTipo() {
        return idTipo;
    }
    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }
    public String getDataInicio() {
        return dataInicio;
    }
    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }
    public String getDataFim() {
        return dataFim;
    }
    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }
    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public String getDescricao() {
        return descricao;
    }
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getIdServico() {
        return idServico;
    }

    public void setIdServico(int idServico) {
        this.idServico = idServico;
    }

}

