package model;


import java.util.ArrayList;
import java.util.List;

public class Agenda
{
    String data;
    List<Agendamento> listAgendamento = new ArrayList<Agendamento>();

    public Agenda(String data, List<Agendamento> listAgendamento)
    {
        super();
        this.data = data;
        this.listAgendamento = listAgendamento;
    }

    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }
    public List<Agendamento> getListAgendamento() {
        return listAgendamento;
    }
    public void setListAgendamento(List<Agendamento> listAgendamento) {
        this.listAgendamento = listAgendamento;
    }
}
