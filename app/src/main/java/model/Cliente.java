package model;

import java.io.Serializable;

/**
 * Created by Junior on 03/11/2014.
 */
public class Cliente extends Usuario implements Serializable
{
    int id;
    int idUsuario;

    public Cliente()
    {

    }

    public Cliente(String numCPF,
                   String numCNPJ,
                   String nome,
                   String dtaNascimento,
                   String apelido,
                   String senha,
                   String email,
                   String telefone,
                   String celular)
    {
        super(numCPF,
            numCNPJ,
            nome,
            dtaNascimento,
            apelido,
            senha,
            email,
            telefone,
            celular);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
}
