package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by williampietro on 11/09/14.
 */
public class CategoriaServicos {
    int id;
    String descricao;
    boolean urgente;
    List<Subcategoria> subcategorias = new ArrayList<Subcategoria>();

    public CategoriaServicos(int id, String descricao, boolean urgente,List<Subcategoria> subcategorias) {
        this.id = id;
        this.descricao = descricao;
        this.urgente = urgente;
        this.subcategorias = subcategorias;
    }

    @Override
    public String toString() {
        return "CategoriaServicos{" +
                "id=" + id +
                ", descricao='" + descricao + '\'' +
                ", subcategorias=" + subcategorias.toString() +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean getUrgente(){return urgente;}

    public void setUrgente(boolean urgente){ this.urgente = urgente; }

    public List<Subcategoria> getSubcategorias() {
        return subcategorias;
    }

    public void setSubcategorias(List<Subcategoria> subcategorias) {
        this.subcategorias = subcategorias;
    }
}
