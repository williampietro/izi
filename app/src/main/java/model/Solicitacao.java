package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by williampietro on 12/09/14.
 */
public class Solicitacao implements Serializable
{


    int id;
    private String dataHora;
    private String titulo, descricao;
    private int idEndereco, idSubcategoria, idCategoria, idStatus;

    private List<Orcamento> lstOrcamento;
    private Cliente cliente;
    private Endereco endereco;

    public Solicitacao(){}

    public Solicitacao(int id, String dataHora, String titulo, String descricao,
                       int idEndereco, int idSubcategoria, int idCategoria, int idStatus) {
        super();
        this.id = id;
        this.dataHora = dataHora;
        this.titulo = titulo;
        this.descricao = descricao;
        this.idEndereco = idEndereco;
        this.idSubcategoria = idSubcategoria;
        this.idCategoria = idCategoria;
        this.idStatus = idStatus;
    }
    public Solicitacao(String dataHora, String titulo, String descricao,
                       int idEndereco, int idSubcategoria, int idCategoria, int idStatus) {
        super();
        this.dataHora = dataHora;
        this.titulo = titulo;
        this.descricao = descricao;
        this.idEndereco = idEndereco;
        this.idSubcategoria = idSubcategoria;
        this.idCategoria = idCategoria;
        this.idStatus = idStatus;
    }
    @Override
    public String toString() {
        return "Solicitacao [id=" + id + ", dataHora=" + dataHora + ", titulo="
                + titulo + ", descricao=" + descricao + ", idEndereco="
                + idEndereco + ", idSubcategoria=" + idSubcategoria
                + ", idCategoria=" + idCategoria + ", idStatus=" + idStatus
                + "]";
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getDataHora() {
        return dataHora;
    }
    public void setDataHora(String dataHora) {
        this.dataHora = dataHora;
    }
    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public String getDescricao() {
        return descricao;
    }
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    public int getIdEndereco() {
        return idEndereco;
    }
    public void setIdEndereco(int idEndereco) {
        this.idEndereco = idEndereco;
    }
    public int getIdSubcategoria() {
        return idSubcategoria;
    }
    public void setIdSubcategoria(int idSubcategoria) {
        this.idSubcategoria = idSubcategoria;
    }
    public int getIdCategoria() {
        return idCategoria;
    }
    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }
    public int getIdStatus() {
        return idStatus;
    }
    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public List<Orcamento> getLstOrcamento() {
        return lstOrcamento;
    }

    public void setLstOrcamento(List<Orcamento> lstOrcamento) {
        this.lstOrcamento = lstOrcamento;
    }
}
