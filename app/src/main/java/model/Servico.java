package model;

/**
 * Created by Junior on 04/11/2014.
 */
public class Servico
{
    private String  numValor,
                    desServico,
                    desTitulo,
                    dataInicio,
                    dataFim;
    private int     numAvaliacaoCliente,
                    numAvaliacaoFornecedor,
                    idStatusServico;

    private Orcamento orcamento;
    private Agendamento agendamento;

    public Servico()
    {

    }

    public Servico(String numValor,
                   String desServico,
                   int numAvaliacaoCliente,
                   int numAvaliacaoFornecedor,
                   int idStatusServico,
                   Orcamento orcamento,
                   Agendamento agendamento)
    {
        this.numValor = numValor;
        this.desServico = desServico;
        this.numAvaliacaoCliente = numAvaliacaoCliente;
        this.numAvaliacaoFornecedor = numAvaliacaoFornecedor;
        this.idStatusServico = idStatusServico;
        this.orcamento = orcamento;
        this.agendamento = agendamento;
    }

    public String getNumValor() {
        return numValor;
    }

    public void setNumValor(String numValor) {
        this.numValor = numValor;
    }

    public String getDesServico() {
        return desServico;
    }

    public void setDesServico(String desServico) {
        this.desServico = desServico;
    }

    public int getNumAvaliacaoCliente() {
        return numAvaliacaoCliente;
    }

    public void setNumAvaliacaoCliente(int numAvaliacaoCliente) {
        this.numAvaliacaoCliente = numAvaliacaoCliente;
    }

    public int getNumAvaliacaoFornecedor() {
        return numAvaliacaoFornecedor;
    }

    public void setNumAvaliacaoFornecedor(int numAvaliacaoFornecedor) {
        this.numAvaliacaoFornecedor = numAvaliacaoFornecedor;
    }

    public int getIdStatusServico() {
        return idStatusServico;
    }

    public void setIdStatusServico(int idStatusServico) {
        this.idStatusServico = idStatusServico;
    }

    public Orcamento getOrcamento() {
        return orcamento;
    }

    public void setOrcamento(Orcamento orcamento) {
        this.orcamento = orcamento;
    }

    public Agendamento getAgendamento() {
        return agendamento;
    }

    public void setAgendamento(Agendamento agendamento) {
        this.agendamento = agendamento;
    }

    public String getDesTitulo() {
        return desTitulo;
    }

    public void setDesTitulo(String desTitulo) {
        this.desTitulo = desTitulo;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }
}
