package model;

import java.io.Serializable;

/**
 * Created by williampietro on 09/09/14.
 */
public class Endereco implements Serializable {

    protected int sequencia = 0;
    protected int id;
    protected String cep, logradouro, numero, complemento, bairro, cidade, uf;
    protected double latitude, longitude;

    public Endereco() {}

    public Endereco( String cep, String logradouro, String numero, String complemento, String bairro, String cidade, String uf, double latitude, double longitude) {
        this.cep = cep;
        this.logradouro = logradouro;
        this.numero = numero;
        this.complemento = complemento;
        this.bairro = bairro;
        this.cidade = cidade;
        this.uf = uf;
        this.latitude = latitude;
        this.longitude = longitude;
        //enderecoes.add(this);
    }
    public Endereco( int id,String cep, String logradouro, String numero, String complemento, String bairro, String cidade, String uf, double latitude, double longitude) {
        this.id = id;
        this.cep = cep;
        this.logradouro = logradouro;
        this.numero = numero;
        this.complemento = complemento;
        this.bairro = bairro;
        this.cidade = cidade;
        this.uf = uf;
        this.latitude = latitude;
        this.longitude = longitude;
        //enderecoes.add(this);
    }

    @Override
    public String toString() {
        return "Log: " +  logradouro + "," + numero + " - " + complemento +" CEP: " +
                cep + ", Bairro: " + bairro + " - " + cidade + " - " + uf;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(long latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(long longitude) {
        this.longitude = longitude;
    }
}
