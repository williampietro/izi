package model;

import java.io.Serializable;

/**
 * Created by Junior on 03/11/2014.
 */
public class Orcamento implements Serializable
{
    private int id,
                idStatus,
                idFornecedor;

    private String  vlrTotalServico,
                    desOrcamento;

    private Solicitacao solicitacao;
    private Agendamento agendamento;

    public Orcamento()
    {

    }

    public Orcamento(String vlrTotalServico,
                     String desOrcamento,
                     int idStatus,
                     int idFornecedor,
                     Solicitacao solicitacao,
                     Agenda agenda)
    {
        this.vlrTotalServico = vlrTotalServico;
        this.desOrcamento = desOrcamento;
        this.idStatus = idStatus;
        this.idFornecedor = idFornecedor;
        this.solicitacao = solicitacao;
        this.agendamento = agendamento;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVlrTotalServico() {
        return vlrTotalServico;
    }

    public void setVlrTotalServico(String vlrTotalServico) {
        this.vlrTotalServico = vlrTotalServico;
    }

    public String getDesOrcamento() {
        return desOrcamento;
    }

    public void setDesOrcamento(String desOrcamento) {
        this.desOrcamento = desOrcamento;
    }

    public Solicitacao getSolicitacao() {
        return solicitacao;
    }

    public void setSolicitacao(Solicitacao solicitacao) {
        this.solicitacao = solicitacao;
    }

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public Agendamento getAgendamento() {
        return agendamento;
    }

    public void setAgendamento(Agendamento agendamento) {
        this.agendamento = agendamento;
    }

    public int getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(int idFornecedor) {
        this.idFornecedor = idFornecedor;
    }
}
