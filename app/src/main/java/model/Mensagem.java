package model;

import java.util.Date;

/**
 * Created by williampietro on 06/11/14.
 */
public class Mensagem {

    int id, idUsuario;
    String texto;
    Date data;

    public Mensagem(){}
    public Mensagem(int idUsuario, String texto, Date data) {
        this.idUsuario = idUsuario;
        this.texto = texto;
        this.data = data;
    }

    @Override
    public String toString() {
        return data + " - " + texto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
