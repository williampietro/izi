package model;

import java.io.Serializable;

/**
 * Created by williampietro on 04/11/14.
 */
public class ItemServico implements Serializable{
    String tipo;
    String titulo;
    String status;
    String desCliente;
    String desFornecedor;
    int idCategoria, idOrcamento, idServico, idFornecedor, idCliente, idSolicitacao;

    public ItemServico(String tipo, String titulo, String status, int idCategoria, int idOrcamento, int idServico, int idFornecedor, int idCliente, int idSolicitacao) {
        this.tipo = tipo;
        this.titulo = titulo;
        this.status = status;
        this.idCategoria = idCategoria;
        this.idOrcamento = idOrcamento;
        this.idServico = idServico;
        this.idFornecedor = idFornecedor;
        this.idCliente = idCliente;
        this.idSolicitacao = idSolicitacao;
    }
    public ItemServico(){}

    @Override
    public String toString() {
        return "ItemServico{" +
                "tipo='" + tipo + '\'' +
                ", titulo='" + titulo + '\'' +
                ", status='" + status + '\'' +
                ", idCategoria=" + idCategoria +
                ", idOrcamento=" + idOrcamento +
                ", idServico=" + idServico +
                ", idFornecedor=" + idFornecedor +
                ", idCliente=" + idCliente +
                '}';
    }
    public String getDesFornecedor() {
        return desFornecedor;
    }

    public void setDesFornecedor(String desFornecedor) {
        this.desFornecedor = desFornecedor;
    }

    public String getDesCliente() {
        return desCliente;
    }

    public void setDesCliente(String desCliente) {
        this.desCliente = desCliente;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public int getIdOrcamento() {
        return idOrcamento;
    }

    public void setIdOrcamento(int idOrcamento) {
        this.idOrcamento = idOrcamento;
    }

    public int getIdServico() {
        return idServico;
    }

    public void setIdServico(int idServico) {
        this.idServico = idServico;
    }

    public int getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(int idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdSolicitacao() {
        return idSolicitacao;
    }

    public void setIdSolicitacao(int idSolicitacao) {
        this.idSolicitacao = idSolicitacao;
    }
}
