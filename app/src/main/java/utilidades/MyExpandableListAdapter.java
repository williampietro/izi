package utilidades;

/**
 * Created by williampietro on 08/11/14.
 */

import android.app.Activity;
import android.content.Intent;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import model.Agenda;
import model.ItemServico;
import pf2.snap.AnaliseServicoActivity;
import pf2.snap.AprovarOrcamentoActivity;
import pf2.snap.ListaAgendaActivity;
import pf2.snap.ListaEnderecoActivity;
import pf2.snap.NovaAgendaActivity;
import pf2.snap.R;

import static android.support.v4.app.ActivityCompat.startActivity;

public class MyExpandableListAdapter extends BaseExpandableListAdapter {

    private final SparseArray<Group> groups;
    private final List<Agenda> listAgenda;
    public LayoutInflater inflater;
    public Activity activity;

    public MyExpandableListAdapter(Activity act, SparseArray<Group> groups, List<Agenda> listAgenda) {
        activity = act;
        this.groups = groups;
        inflater = act.getLayoutInflater();
        this.listAgenda = listAgenda;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groups.get(groupPosition).children.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String children = (String) getChild(groupPosition, childPosition);
        TextView text = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listrow_details, null);
        }
        text = (TextView) convertView.findViewById(R.id.textView1);
        text.setText(children);
        convertView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();

                ItemServico item = new ItemServico();
                if(listAgenda.get(groupPosition).getListAgendamento().get(childPosition).getIdTipo() == 1){
                    item.setTipo("Orçamento");
                }else if(listAgenda.get(groupPosition).getListAgendamento().get(childPosition).getIdTipo() == 2){
                    item.setTipo("Serviço");
                }else{
                    item.setTipo("Pessoal");
                }
                item.setTitulo(listAgenda.get(groupPosition).getListAgendamento().get(childPosition).getTitulo());
                item.setIdOrcamento(listAgenda.get(groupPosition).getListAgendamento().get(childPosition).getIdOrcamento());
                item.setIdServico(listAgenda.get(groupPosition).getListAgendamento().get(childPosition).getIdServico());

                if(listAgenda.get(groupPosition).getListAgendamento().get(childPosition).getIdTipo() == 1){
                    //Chama Orcamento -> transformar em ItemServico
                    intent = new Intent(activity, AprovarOrcamentoActivity.class);
                    intent.putExtra("item", item);
                }else if (listAgenda.get(groupPosition).getListAgendamento().get(childPosition).getIdTipo() == 2){
                    //Chama Servico -> transformar em ItemServico
                    //intent = new Intent(activity, .class);
                    intent.putExtra("item", item);
                }else {
                    intent = new Intent(activity, NovaAgendaActivity.class);
                    intent.putExtra("agendamento", listAgenda.get(groupPosition).getListAgendamento().get(childPosition));
                }
                activity.startActivity(intent);
                }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return groups.get(groupPosition).children.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listrow_group, null);
        }
        Group group = (Group) getGroup(groupPosition);
        ((CheckedTextView) convertView).setText(group.string);
        ((CheckedTextView) convertView).setChecked(isExpanded);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}