package utilidades;

import com.android.volley.VolleyError;

/**
 * Created by williampietro on 24/10/14.
 */
public interface PostCommentResponseListener {
    public void requestStarted();
    public void requestCompleted();
    public void requestEndedWithError(VolleyError error);
}
