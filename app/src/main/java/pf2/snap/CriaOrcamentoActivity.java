package pf2.snap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.security.Security;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import model.Agenda;
import model.Agendamento;
import model.Orcamento;
import model.Solicitacao;
import utilidades.Mask;
import utilidades.Sessao;


public class CriaOrcamentoActivity extends Activity
{
    boolean cancel = false;
    String urlOrc;
    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cria_orcamento);
        urlOrc = "http://54.69.232.33:8080/webService_Snap/rest/orcamento/";

        //Captura intent da solicitacao
        Intent intent = getIntent();
        final Solicitacao solicitacao = (Solicitacao) intent.getSerializableExtra("solicitacao");

        TextView txtTituloOrcamento = (TextView) findViewById(R.id.lblTituloDoServico);
        TextView txtTituloSolicitacao = (TextView) findViewById(R.id.lblTituloDoServico);

        final EditText txtDescOrcamento = (EditText) findViewById(R.id.txtDescOrcamento);
        final EditText txtValorOrcamento = (EditText) findViewById(R.id.txtValorOrcamento);
        final EditText txtDataInicio = (EditText) findViewById(R.id.txtDataInicioOrcamento);
        final EditText txtDataFim = (EditText) findViewById(R.id.txtDataFimOrcamento);
        Button btnEnviaOrcamento = (Button) findViewById(R.id.btnEnviaOrcamento);

        //txtTituloOrcamento.setText();
        txtDataInicio.addTextChangedListener(Mask.insert("##/##/####", txtDataInicio));
        txtDataFim.addTextChangedListener(Mask.insert("##/##/####", txtDataFim));

        txtTituloSolicitacao.setText(solicitacao.getTitulo().toString());
        txtDataInicio.setText(solicitacao.getDataHora().toString());


        btnEnviaOrcamento.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Faz validação dos campos antes da ação.
                if (!validacao())
                {
                    try
                    {
                        final Agendamento agendamento =  new Agendamento();

                        agendamento.setDataFim(txtDataFim.getText().toString());
                        agendamento.setDataInicio(txtDataInicio.getText().toString());
                        agendamento.setIdTipo(1);//Tipo 1 significa Orçamento conforme TAB_Tipo_Agenda

                        final Orcamento orcamento = new Orcamento();

                        orcamento.setIdStatus(1);//Status 1 significa Aguardando Execução conforme TAB_Status_Orcamento
                        orcamento.setAgendamento(agendamento);
                        orcamento.setDesOrcamento(txtDescOrcamento.getText().toString());
                        orcamento.setIdFornecedor(Sessao.getInstance().getIdFornecedor());
                        orcamento.setSolicitacao(solicitacao);
                        orcamento.setVlrTotalServico(txtValorOrcamento.getText().toString());

                        gravaOrcamento(orcamento, agendamento);

                        Toast.makeText(CriaOrcamentoActivity.this, "Orçamento Enviado", Toast.LENGTH_LONG).show();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    protected boolean validacao()
    {
        cancel = false;
        View focusView = null;

        EditText txtDescOrcamento = (EditText) findViewById(R.id.txtDescOrcamento);
        EditText txtValorOrcamento = (EditText) findViewById(R.id.txtValorOrcamento);
        EditText txtDataInicio = (EditText) findViewById(R.id.txtDataInicioOrcamento);
        EditText txtDataFim = (EditText) findViewById(R.id.txtDataFimOrcamento);

        if (TextUtils.isEmpty(txtDescOrcamento.getText().toString())) {
            txtDescOrcamento.setError(getString(R.string.error_field_required));
            focusView = txtDescOrcamento;
            cancel = true;
        }

        if (TextUtils.isEmpty(txtValorOrcamento.getText().toString())) {
            txtValorOrcamento.setError(getString(R.string.error_field_required));
            focusView = txtValorOrcamento;
            cancel = true;
        }

        if (TextUtils.isEmpty(txtDataInicio.getText().toString())) {
            txtDataInicio.setError(getString(R.string.error_field_required));
            focusView = txtDataInicio;
            cancel = true;
        }
        if (TextUtils.isEmpty(txtDataFim.getText().toString())) {
            txtDataFim.setError(getString(R.string.error_field_required));
            focusView = txtDataFim;
            cancel = true;
        }

        if (txtDescOrcamento.getText().toString().contentEquals(" ")) {
            txtDescOrcamento.setError(getString(R.string.error_valor_inválido));
            focusView = txtDescOrcamento;
            cancel = true;
        }
        //TODO testar se datas não são invertidas.


        if (cancel) {
            focusView.requestFocus();
        } else {
            cancel = false;
        }
        return cancel;

    }

    public void gravaOrcamento(final Orcamento orcamento, final Agendamento agendamento)
    {
        queue = Volley.newRequestQueue(this);
        StringRequest jsonObjReqOrcamento = new StringRequest(Request.Method.POST, urlOrc + "add",new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                Log.d("Post", response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                VolleyLog.d("Post", "Error: " + error.getMessage());
            }
        }){

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("id", String.valueOf(orcamento.getId()));
                params.put("valor", String.valueOf(orcamento.getVlrTotalServico()));
                params.put("descricao", String.valueOf(orcamento.getDesOrcamento()));
                params.put("dataInicio", String.valueOf(agendamento.getDataInicio()));
                params.put("dataFim", String.valueOf(agendamento.getDataFim()));
                params.put("solicitacao", String.valueOf(orcamento.getSolicitacao().getId()));
                params.put("fornecedor", String.valueOf(Sessao.getInstance().getIdFornecedor()));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };
        queue.add(jsonObjReqOrcamento);
    }
}
