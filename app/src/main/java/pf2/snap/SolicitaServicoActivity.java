package pf2.snap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import model.CategoriaServicos;
import model.Endereco;
import model.Solicitacao;
import model.Subcategoria;
import utilidades.Mask;

import static android.util.Pair.*;


public class SolicitaServicoActivity extends Activity {
    int idCategoria,idSubcategoria, posicaoLista;
    boolean cancel = false;
    boolean validaCategoria = false;
    String titulo;
    String idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicita_servico);

        //cria objetos da View
        //final EditText txtTituloServico = (EditText) findViewById(R.id.txtTituloServico);
        final EditText txtDescricaoServico = (EditText) findViewById(R.id.txtDescricaoServico);
        final EditText txtDataOrcamento = (EditText) findViewById(R.id.txtDataOrcamento);
        final EditText txtHoraOrcamento = (EditText) findViewById(R.id.txtHoraOrcamento);
        final ProgressDialog pDialog = new ProgressDialog(this);

        final RequestQueue queue = Volley.newRequestQueue(this);
        final String url = "http://54.69.232.33:8080/webService_Snap/rest/solicitacao/";


        pDialog.setMessage("Aguarde...");

        Intent intent = getIntent();
        ArrayList array = (ArrayList)intent.getSerializableExtra("IdUsuarioIdEndereco");
        idUsuario = String.valueOf(array.get(0));
        Endereco end = new Endereco();
        end.setId((Integer) array.get(1));
        final int idEndereco = end.getId();

        final ArrayList<CategoriaServicos> listCategoria = new ArrayList<CategoriaServicos>();

        //Adiciona mascaras aos campos que necessitam
        txtDataOrcamento.addTextChangedListener(Mask.insert("##/##/####", txtDataOrcamento));
        txtHoraOrcamento.addTextChangedListener(Mask.insert("##:##", txtHoraOrcamento));

        //Spinner categoria
        final ArrayList<String> lstCategoriaToView = new ArrayList<String>();
        lstCategoriaToView.add("Selecione uma categoria");
        final Spinner spnCategoria = (Spinner) findViewById(R.id.spinnerCategoria);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,lstCategoriaToView);
        ArrayAdapter<String> spinnerArrayAdapter = arrayAdapter;


        //busca as categorias
        JsonArrayRequest getRequest = new JsonArrayRequest("http://54.69.232.33:8080/webService_Snap/rest/categoria/listar",
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        // display response
                        Log.d("Response", response.toString());
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject cat = response.getJSONObject(i);
                                ArrayList<Subcategoria> listsubcat = new ArrayList<Subcategoria>();
                                JSONArray subcat = cat.getJSONArray("subcategorias");
                                for (int x = 0 ; x <subcat.length(); x++) {
                                    Subcategoria subcategoria = new Subcategoria(Integer.valueOf(subcat.getJSONObject(x).getString("id")),
                                            subcat.getJSONObject(x).getString("descricao"));
                                    listsubcat.add(subcategoria);
                                }
                                CategoriaServicos categoria = new CategoriaServicos(Integer.valueOf(cat.getString("id")),
                                        cat.getString("descricao"),
                                        Boolean.valueOf(cat.getString("urgente")),listsubcat);
                                if(!categoria.getUrgente()) {
                                    listCategoria.add(categoria);
                                    lstCategoriaToView.add(categoria.getDescricao());
                                    arrayAdapter.notifyDataSetChanged();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        pDialog.hide();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("Error.Response", error.getMessage());
        pDialog.hide();
    }
}
        );
        // add it to the RequestQueue
        queue.add(getRequest);


        //Spinner para subcategoria.
        final ArrayList<String> lstSubcategoriaToView = new ArrayList<String>();
        lstSubcategoriaToView.add("Selecione um título..");
        final Spinner spnSubcategoria = (Spinner) findViewById(R.id.spinnerSubcategoria);
        final ArrayAdapter<String> arrayAdapterSub = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,lstSubcategoriaToView);
        ArrayAdapter<String> spinnerArrayAdapterSub = arrayAdapterSub;
        spinnerArrayAdapterSub.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spnSubcategoria.setAdapter(spinnerArrayAdapterSub);


        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spnCategoria.setAdapter(spinnerArrayAdapter);
        spnCategoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int posicao, long id) {
                lstSubcategoriaToView.clear();
                lstSubcategoriaToView.add("Selecione um título..");

                //Altera subcategoria
                try {
                    //pega a categoria pela posição
                    idCategoria = listCategoria.get(posicao - 1).getId();
                    posicaoLista = posicao-1;
                    for (int i = 0 ; i < listCategoria.get(posicao-1).getSubcategorias().size() ; i++) {
                        lstSubcategoriaToView.add(listCategoria.get(posicao-1).getSubcategorias().get(i).getDescricao());
                    }
                    arrayAdapterSub.notifyDataSetChanged();

                }catch (Exception e){
                    e.printStackTrace();
                }

                //Valida: se for categoria "Selecione", atualiza variavel usada no validacao()
                if (parent.getSelectedItemPosition() == 0) {
                    validaCategoria = true;
                } else {
                    validaCategoria = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                validaCategoria = true;
            }
        });

        spnSubcategoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int posicao, long id) {
                try {
                    idSubcategoria = listCategoria.get(posicaoLista).getSubcategorias().get(posicao-1).getId();
                    titulo = parent.getItemAtPosition(posicao).toString();
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        Button btnSolicitaServico = (Button) findViewById(R.id.btnEnviarSolicitacao);
        btnSolicitaServico.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!validacao())
                {
                    //cria string com data e hora da tela + HardCode de segundos.
                    final String dataHora = txtDataOrcamento.getText().toString() + " " + txtHoraOrcamento.getText().toString() + ":00";
                    /*Date dataHora = null;

                    try
                    {
                        DateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        dataHora = fmt.parse(dataString);
                    }
                    catch (ParseException e)
                    {
                        e.printStackTrace();
                    }*/

                    final Solicitacao solicitacao = new Solicitacao(dataHora,titulo, txtDescricaoServico.getText().toString(),
                            idEndereco, idSubcategoria,idCategoria,1);

                    StringRequest jsonObjReq = new StringRequest(Request.Method.POST, url + "add",
                            new Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    Log.d("Post", response.toString());
                                    Toast.makeText(SolicitaServicoActivity.this, "Serviço gravado com sucesso!", Toast.LENGTH_LONG).show();
                                    chamaProximaTela();
                                    pDialog.hide();
                                }
                            }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyLog.d("Post", "Error: " + error.getMessage());
                            pDialog.hide();
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();

                            params.put("idUsuario", idUsuario);
                            params.put("id", String.valueOf(solicitacao.getId()));
                            params.put("titulo", solicitacao.getTitulo());
                            params.put("descricao", solicitacao.getDescricao());
                            params.put("idCategoria", String.valueOf(solicitacao.getIdCategoria()));
                            params.put("datahora",dataHora);
                            params.put("idEndereco", String.valueOf(solicitacao.getIdEndereco()));
                            params.put("idSubcategoria",String.valueOf(solicitacao.getIdSubcategoria()));
                            params.put("idStatus",String.valueOf(solicitacao.getIdStatus()));
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Content-Type", "application/x-www-form-urlencoded");
                            return params;
                        }

                    };
                    queue.add(jsonObjReq);
                }
            }
        });

    }

    public void chamaProximaTela(){
        Intent intent = new Intent(this, RodadorActivity.class);
        // 3. put person in intent data
        intent.putExtra("idUsuario", Integer.valueOf(idUsuario));
        // 4. start the activity
        startActivity(intent);
    }

    protected boolean validacao(){

        cancel = false;
        View focusView = null;
        final EditText txtDescricaoServico = (EditText) findViewById(R.id.txtDescricaoServico);
        final EditText txtDataOrcamento = (EditText) findViewById(R.id.txtDataOrcamento);
        final EditText txtHoraOrcamento = (EditText) findViewById(R.id.txtHoraOrcamento);

        Date dateTela = new Date();
        Date dateAtual = new Date();
        Date dateFuturo = new Date();

        //dateFuturo = dateFuturo.
        try
        {
            DateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
            dateTela = fmt.parse(txtDataOrcamento.toString());
            dateAtual = fmt.parse(txtDataOrcamento.toString());
            dateFuturo = fmt.parse(txtDataOrcamento.toString());

            //dateTela = (Date)dateTela;


        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        System.out.println(dateTela + " - " + dateAtual + " - " + dateFuturo + " - " + txtDataOrcamento.getText());

        if(dateTela.before(dateAtual) || dateTela.after(dateFuturo))
        {
            Toast.makeText(SolicitaServicoActivity.this, "Selecione uma Válida.", Toast.LENGTH_SHORT).show();
            cancel = true;
        }


        if(validaCategoria){
            Toast.makeText(SolicitaServicoActivity.this, "Selecione uma categoria!", Toast.LENGTH_SHORT).show();
            cancel = true;
            focusView = txtDescricaoServico;
        }

        if (TextUtils.isEmpty(txtDataOrcamento.getText().toString())) {
            txtDataOrcamento.setError(getString(R.string.error_field_required));
            focusView = txtDataOrcamento;
            cancel = true;
        }

        if (TextUtils.isEmpty(txtHoraOrcamento.getText().toString())) {
            txtHoraOrcamento.setError(getString(R.string.error_field_required));
            focusView = txtHoraOrcamento;
            cancel = true;
        }
            if (cancel) {
                focusView.requestFocus();
            } else {
                cancel = false;
            }
            return cancel;
        }

}
