package pf2.snap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import model.Cliente;
import model.Endereco;
import model.ItemServico;
import model.Solicitacao;
import utilidades.Sessao;


public class AvaliacaoActivity extends Activity {
    final String urlSol = "http://54.69.232.33:8080/webService_Snap/rest/servicos/";
    RequestQueue queue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avaliacao);

        queue = Volley.newRequestQueue(this);

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Aguarde...");
        pDialog.show();

        final TextView txtTitulo = (TextView) findViewById(R.id.lblTituloAvaliacao);
        final TextView txtClienteFornecedor = (TextView) findViewById(R.id.lblClienteFornecedorAvaliacao);
        final TextView txtTipoServico = (TextView) findViewById(R.id.lblTipoServicoAvaliacao);
        final EditText txtNota = (EditText) findViewById(R.id.txtNota);

        Intent intent = getIntent();
        final ItemServico item = (ItemServico) intent.getSerializableExtra("item");

        final int idServico = item.getIdServico();

        JsonArrayRequest getRequest = new JsonArrayRequest(urlSol + "buscarPorId/" + idServico,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response)
                    {
                        // display response
                        Log.d("Response", response.toString());
                        for (int i = 0; i < response.length(); i++)
                        {
                            try
                            {
                                JSONObject obj = response.getJSONObject(i);

                                txtTitulo.setText(obj.getString("desTitulo"));
                                txtTipoServico.setText("Descrição Servico: " + obj.getString("desServico"));
                                if(Sessao.getInstance().getIdFornecedor() != 0) {
                                    txtClienteFornecedor.setText("Cliente: " + obj.getString("desCliente"));
                                }else{
                                    txtClienteFornecedor.setText("Fornecedor: " + obj.getString("desFornecedor"));
                                }

                                pDialog.hide();
                                onRestart();
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.d("Error.Response", error.getMessage());
                    }
                }
        );
        // add it to the RequestQueue
        queue.add(getRequest);

        Button btnAvaliar = (Button) findViewById(R.id.btnAvaliar);
        btnAvaliar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View focusView;

                if(TextUtils.isEmpty(txtNota.getText().toString())) {
                    txtNota.setError(getString(R.string.error_field_required));
                    focusView = txtNota;
                    focusView.requestFocus();
                }else if(Integer.valueOf(txtNota.getText().toString()) < 0 ||
                Integer.valueOf(txtNota.getText().toString()) > 10){
                    txtNota.setError("Valor inválido");
                    focusView = txtNota;
                    focusView.requestFocus();
                }else{
                    StringRequest jsonObjReq = new StringRequest(Request.Method.POST, urlSol + "avaliar",
                            new Response.Listener<String>() {

                                @Override
                                public void onResponse(String response) {
                                    Log.d("Post", response.toString());

                                }
                            }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            VolleyLog.d("Post", "Error: " + error.getMessage());

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("idServico",String.valueOf(idServico));
                            if(Sessao.getInstance().getIdFornecedor() != 0){
                                params.put("numAvaliacaoCliente",txtNota.getText().toString());
                            }else{
                                params.put("numAvaliacaoFornecedor",txtNota.getText().toString());
                            }

                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Content-Type", "application/x-www-form-urlencoded");
                            return params;
                        }

                    };

                    // Adding request to request queue
                    //AppController.getInstance().addToRequestQueue(jsonObjReq);
                    queue.add(jsonObjReq);

                }
            }
        });
    }
}
