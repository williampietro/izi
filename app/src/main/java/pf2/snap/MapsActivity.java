package pf2.snap;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import model.Endereco;
import utilidades.Sessao;

public class MapsActivity extends FragmentActivity{

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    LocationManager mLocationManager;
    LatLng minhaLatLng;

    MarkerOptions markerOptions;
    Location myLocation;
    boolean primeiraExec;
    int idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        //Intent intent = getIntent();
        //idUsuario =  (Integer) intent.getSerializableExtra("idUsuario");
        idUsuario = Sessao.getInstance().getIdUsuario();
        final EditText txtEndereço = (EditText) findViewById(R.id.txtEndereco);
        primeiraExec = true;
        setUpMapIfNeeded();
        Geocoder geocoder = new Geocoder(MapsActivity.this,Locale.getDefault());
        try {
            txtEndereço.setText(
                    geocoder.getFromLocation(minhaLatLng.latitude, minhaLatLng.longitude, 1).get(0).getAddressLine(0));
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng arg0) {
                //Pega latitude e longitude do local clicado
                minhaLatLng = arg0;
                //Limpa markers antigos
                mMap.clear();
                //Anima camera até o ponto clicado
                mMap.animateCamera(CameraUpdateFactory.newLatLng(minhaLatLng));
                //Cria o marcador
                markerOptions = new MarkerOptions();
                //Ajusta o marcador criado para a posição clicada
                markerOptions.position(minhaLatLng);
                //Adiciona o marcador ao mapa
                mMap.addMarker(markerOptions);
                //chama o metodo para buscar o endereço a partir de latitude e longitude.
                new ReverseGeocodingTask(getBaseContext()).execute(minhaLatLng);
            }
        });

        //busca endereço qndo aperta enter ou minimiza o teclado
        txtEndereço.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                                actionId == EditorInfo.IME_ACTION_DONE ||
                                event.getAction() == KeyEvent.ACTION_DOWN &&
                                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            if (!event.isShiftPressed()) {
                                minhaLatLng = getLocationFromAddress(txtEndereço.getText().toString());
                                setUpMap();
                                return true; // consume.
                            }
                        }
                        return false; // pass on to other listeners.
                    }
                });

        Button btnOkEndereco = (Button) findViewById(R.id.btnOkEndereco);
        btnOkEndereco.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                criaEndereco(txtEndereço.getText().toString());
            }
        });

    }//Fim do OnCreate


    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }

        }
    }

    private void setUpMap() {
        if(primeiraExec){
            mMap.setMyLocationEnabled(true);
            myLocation = getLastKnownLocation();
            //TODO buscar localizacao atualizada, esta com workaround pra latlng 0,0
            if(myLocation != null){
                minhaLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            }
            else{
                minhaLatLng = new LatLng(-25.4200388,-49.2650973);
            }
            //Fim do tudu
            primeiraExec = false;
        }
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(minhaLatLng).title("Você!"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(minhaLatLng, 17));
    }

    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    private class ReverseGeocodingTask extends AsyncTask<LatLng, Void, String> {
        Context mContext;

        public ReverseGeocodingTask(Context context){
            super();
            mContext = context;
        }

        // Encontrando endereço por geocoding
        @Override
        protected String doInBackground(LatLng... params) {
            Geocoder geocoder = new Geocoder(mContext);
            double latitude = params[0].latitude;
            double longitude = params[0].longitude;

            List<Address> addresses = null;
            String addressText="";

            try {
                addresses = geocoder.getFromLocation(latitude, longitude,1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(addresses != null && addresses.size() > 0 ){
                Address address = addresses.get(0);
                addressText = address.getAddressLine(0);
            }

            return addressText;
        }

        @Override
        protected void onPostExecute(String addressText) {
            // Setting the title for the marker.
            // This will be displayed on taping the marker
            markerOptions.title(addressText);
            EditText txtEndereço = (EditText) findViewById(R.id.txtEndereco);
            txtEndereço.setText(addressText);
            // Placing a marker on the touched position
            mMap.addMarker(markerOptions);
        }
    }
    public LatLng getLocationFromAddress(String strAddress) {

            Geocoder coder = new Geocoder(this);
            List<Address> address = null;
            LatLng p1 = null;

            try {
                address = coder.getFromLocationName(strAddress, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(),location.getLongitude());

            return p1;
        }

    private void criaEndereco(String strAddress){
        Geocoder coder = new Geocoder(this);
        List<Address> address = null;
        try {
            address = coder.getFromLocationName(strAddress, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Address location = address.get(0);

        //TODO Implementar logica pra tratar complemento
        //TODO Implementar CASE de Estado para UF
        Endereco endereco = new Endereco(location.getPostalCode(),location.getThoroughfare(),location.getSubThoroughfare()
        ,"", location.getSubLocality(), location.getSubAdminArea(),"",minhaLatLng.latitude, minhaLatLng.longitude);


        Intent intent = new Intent(this, EnderecoActivity.class);
        ArrayList array = new ArrayList();
        array.add(idUsuario);
        array.add(endereco);
        intent.putExtra("IdUsuarioEndereco", array);

        startActivity(intent);

    }
}
