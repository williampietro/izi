package pf2.snap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import model.Endereco;
import model.Usuario;
import utilidades.Mask;


public class EnderecoActivity extends Activity {

    boolean cancel = false;
    boolean edited = false;
    int positionList;
    int idEndereco;
    Endereco enderecoMapa;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_endereco);

        final EditText txtCEP = (EditText) findViewById(R.id.txtCEP);
        final EditText txtLogradouro = (EditText) findViewById(R.id.txtLogradouro);
        final EditText txtNumLogradouro = (EditText) findViewById(R.id.txtNumeroLogradouro);
        final EditText txtComplemento = (EditText) findViewById(R.id.txtComplemento);
        final EditText txtBairro = (EditText) findViewById(R.id.txtBairro);
        final EditText txtCidade = (EditText) findViewById(R.id.txtCidade);
        final Spinner txtUF = (Spinner) findViewById(R.id.txtUF);
        //Cria mascara do CEP
        txtCEP.addTextChangedListener(Mask.insert("##.###-###", txtCEP));
        final ArrayList<String> lstEnderecosToView = new ArrayList<String>();
        final ListView lstViewEnderecos = (ListView) findViewById(R.id.listViewEnderecos);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lstEnderecosToView);
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Aguarde...");

        final Usuario usuario = new Usuario();

        intent = getIntent();
        try {
            usuario.setId((Integer) intent.getSerializableExtra("idUsuario"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList array = (ArrayList) intent.getSerializableExtra("IdUsuarioEndereco");

        if (array != null) {
            usuario.setId((Integer) array.get(0));
            enderecoMapa = (Endereco) array.get(1);
        }

        // seta os campos apenas se a chamada foi feita da mapa
        if (enderecoMapa != null) {
            txtCEP.setText(enderecoMapa.getCep().toString());
            txtLogradouro.setText(enderecoMapa.getLogradouro().toString());
            txtNumLogradouro.setText(enderecoMapa.getNumero().toString());
            txtComplemento.setText(enderecoMapa.getComplemento().toString());
            txtBairro.setText(enderecoMapa.getBairro().toString());
            txtCidade.setText(enderecoMapa.getCidade().toString());
            pDialog.hide();
        }


        final String url = "http://54.69.232.33:8080/webService_Snap/rest/endereco/";
        //final String url = "http://localhost:8080/webService_Snap/rest/endereco/";
        final RequestQueue queue = Volley.newRequestQueue(this);
        pDialog.show();


        JsonArrayRequest getRequest = new JsonArrayRequest(url + "buscar/" + usuario.getId(),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // display response
                        Log.d("Response", response.toString());
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Endereco mEndereco = new Endereco(
                                        obj.getInt("idEndereco"), obj.getString("cep"), obj.getString("logradouro")
                                        , obj.getString("numero"), obj.getString("complemento"), obj.getString("bairro"),
                                        obj.getString("cidade"), obj.getString("uf"), 0, 0);
                                usuario.addEndereco(mEndereco);
                                lstEnderecosToView.add(mEndereco.toString());
                                lstViewEnderecos.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                                pDialog.hide();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        pDialog.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("Error.Response", error.getMessage());
                        pDialog.hide();
                    }
                }
        );
        // add it to the RequestQueue
        queue.add(getRequest);

        /*/ INICIO - Codigo para uso da listView dos enderecos criados.
        for (int i = 0; i < usuario.retornaEndereco().size(); i++) {
            lstEnderecosToView.add(usuario.getPosition(i).toString());
        }
        lstViewEnderecos.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        //FIM - Codigo para uso da listView dos enderecos criados.
        */


        Button btnSalvarEndereco = (Button) findViewById(R.id.btnSalvarEndereco);
        btnSalvarEndereco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Faz validação dos campos antes da ação.
                if (!validacao()) {
                    if (edited == false) {
                        final Endereco novoEndereco = new Endereco(txtCEP.getText().toString(), txtLogradouro.getText().toString(),
                                txtNumLogradouro.getText().toString(), txtComplemento.getText().toString(),
                                txtBairro.getText().toString(), txtCidade.getText().toString(), txtUF.getSelectedItem().toString(), 0, 0);

                        //adiciona o endereco recem adicionado ao listView
                        String endereco = novoEndereco.toString();
                        lstEnderecosToView.add(endereco);
                        adapter.notifyDataSetChanged();

                        //Adiciona o endereço criado a uma list
                        usuario.addEndereco(novoEndereco);

                        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, url + "add",
                                new Response.Listener<String>() {

                                    @Override
                                    public void onResponse(String response) {
                                        Log.d("Post", response.toString());
                                        idEndereco = Integer.valueOf(response.toString());
                                        if (enderecoMapa != null) {
                                            intent = new Intent(EnderecoActivity.this, SolicitaServicoActivity.class);
                                            ArrayList array = new ArrayList();
                                            array.add(usuario.getId());
                                            array.add(idEndereco);
                                            intent.putExtra("IdUsuarioIdEndereco", array);

                                            startActivity(intent);

                                        }
                                        pDialog.hide();
                                    }

                                }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyLog.d("Post", "Error: " + error.getMessage());
                                pDialog.hide();
                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<String, String>();

                                params.put("idPessoa", String.valueOf(usuario.getId()));
                                params.put("idPessoa", String.valueOf(usuario.getId()));
                                params.put("id", String.valueOf(novoEndereco.getId()));
                                params.put("cep", novoEndereco.getCep());
                                params.put("logradouro", novoEndereco.getLogradouro());
                                params.put("numero", novoEndereco.getNumero());
                                params.put("complemento", novoEndereco.getComplemento());
                                params.put("bairro", novoEndereco.getBairro());
                                params.put("cidade", novoEndereco.getCidade());
                                params.put("uf", novoEndereco.getUf());
                                params.put("latitude", String.valueOf(novoEndereco.getLatitude()));
                                params.put("longitude", String.valueOf(novoEndereco.getLongitude()));

                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("Content-Type", "application/x-www-form-urlencoded");
                                return params;
                            }

                        };

                        // Adding request to request queue
                        //AppController.getInstance().addToRequestQueue(jsonObjReq);
                        queue.add(jsonObjReq);

                    } else {

                        usuario.getPosition(positionList).setCep(txtCEP.getText().toString());
                        usuario.getPosition(positionList).setLogradouro(txtLogradouro.getText().toString());
                        usuario.getPosition(positionList).setNumero(txtNumLogradouro.getText().toString());
                        usuario.getPosition(positionList).setComplemento(txtComplemento.getText().toString());
                        usuario.getPosition(positionList).setBairro(txtBairro.getText().toString());
                        usuario.getPosition(positionList).setCidade(txtCidade.getText().toString());
                        lstEnderecosToView.set(positionList, usuario.getPosition(positionList).toString());
                        adapter.notifyDataSetChanged();

                        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, url + "edit",
                                new Response.Listener<String>() {

                                    @Override
                                    public void onResponse(String response) {
                                        Log.d("Post", response.toString());
                                        pDialog.hide();
                                    }
                                }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyLog.d("Post", "Error: " + error.getMessage());
                                pDialog.hide();
                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<String, String>();

                                params.put("id", String.valueOf(usuario.getPosition(positionList).getId()));
                                params.put("cep", usuario.getPosition(positionList).getCep());
                                params.put("logradouro", usuario.getPosition(positionList).getLogradouro());
                                params.put("numero", usuario.getPosition(positionList).getNumero());
                                params.put("complemento", usuario.getPosition(positionList).getComplemento());
                                params.put("bairro", usuario.getPosition(positionList).getBairro());
                                params.put("cidade", usuario.getPosition(positionList).getCidade());
                                params.put("uf", usuario.getPosition(positionList).getUf());
                                params.put("latitude", String.valueOf(usuario.getPosition(positionList).getLatitude()));
                                params.put("longitude", String.valueOf(usuario.getPosition(positionList).getLongitude()));

                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("Content-Type", "application/x-www-form-urlencoded");
                                return params;
                            }

                        };

                        queue.add(jsonObjReq);
                        //Toast.makeText(EnderecoActivity.this, "Chama JSON Edicão", Toast.LENGTH_SHORT).show();
                    }
                    //limpa os campos para entrada de novo endereço
                    txtCEP.setText("");
                    txtLogradouro.setText("");
                    txtNumLogradouro.setText("");
                    txtComplemento.setText("");
                    txtBairro.setText("");
                    txtCidade.setText("");

                    txtCEP.requestFocus();

                    //    Toast.makeText(EnderecoActivity.this, "idCliente = " + idCliente + " getId() " + listEnderecos.get(idCliente).getId(), Toast.LENGTH_SHORT).show();
                }

            }
        });

        lstViewEnderecos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View v, final int position, long id) {
                //Alerta para clique sobre endereços no ListView
                AlertDialog.Builder adb = new AlertDialog.Builder(EnderecoActivity.this);
                adb.setTitle("Escolha uma opção:");
                adb.setMessage("" + lstViewEnderecos.getItemAtPosition(position));
                adb.setPositiveButton("Editar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        positionList = position;
                        txtCEP.setText(usuario.getPosition(position).getCep());
                        txtLogradouro.setText(usuario.getPosition(position).getLogradouro());
                        txtNumLogradouro.setText(usuario.getPosition(position).getNumero());
                        txtComplemento.setText(usuario.getPosition(position).getComplemento());
                        txtBairro.setText(usuario.getPosition(position).getBairro());
                        txtCidade.setText(usuario.getPosition(position).getCidade());
                        edited = true;
                        //Endereco.removeEndereco(position);
                        //lstEnderecosToView.remove(position);
                        //adapter.notifyDataSetChanged();
                        //TODO Buscar no spinner o estado selecionado.
                    }
                });
                //Exclui da lista de endereços e da ListView.
                adb.setNegativeButton("Excluir", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, url + "delete",
                                new Response.Listener<String>() {

                                    @Override
                                    public void onResponse(String response) {
                                        Log.d("Post", response.toString());
                                        pDialog.hide();
                                    }
                                }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyLog.d("Post", "Error: " + error.getMessage());
                                pDialog.hide();
                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("id", String.valueOf(usuario.getPosition(position).getId()));
                                usuario.removeEndereco(position);
                                return params;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("Content-Type", "application/x-www-form-urlencoded");
                                return params;
                            }

                        };

                        // Adding request to request queue
                        //AppController.getInstance().addToRequestQueue(jsonObjReq);
                        queue.add(jsonObjReq);

                        lstEnderecosToView.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });
                adb.show();
            }
        });

    }

    protected boolean validacao() {
        cancel = false;
        View focusView = null;

        final EditText txtCEP = (EditText) findViewById(R.id.txtCEP);
        final EditText txtLogradouro = (EditText) findViewById(R.id.txtLogradouro);
        final EditText txtNumLogradouro = (EditText) findViewById(R.id.txtNumeroLogradouro);
        final EditText txtBairro = (EditText) findViewById(R.id.txtBairro);
        final EditText txtCidade = (EditText) findViewById(R.id.txtCidade);

        if (TextUtils.isEmpty(txtCEP.getText().toString())) {
            txtCEP.setError(getString(R.string.error_field_required));
            focusView = txtCEP;
            cancel = true;
        }


        if (TextUtils.isEmpty(txtLogradouro.getText().toString())) {
            txtLogradouro.setError(getString(R.string.error_field_required));
            focusView = txtLogradouro;
            cancel = true;
        }

        if (TextUtils.isEmpty(txtNumLogradouro.getText().toString())) {
            txtNumLogradouro.setError(getString(R.string.error_field_required));
            focusView = txtNumLogradouro;
            cancel = true;
        }
        if (TextUtils.isEmpty(txtBairro.getText().toString())) {
            txtBairro.setError(getString(R.string.error_field_required));
            focusView = txtBairro;
            cancel = true;
        }
        if (TextUtils.isEmpty(txtCidade.getText().toString())) {
            txtCidade.setError(getString(R.string.error_field_required));
            focusView = txtCidade;
            cancel = true;
        }

        if (txtCEP.getText().toString().length() < 8) {
            txtCEP.setError(getString(R.string.error_cep));
            focusView = txtCEP;
            cancel = true;
        }

        if (txtLogradouro.getText().toString().contentEquals(" ")) {
            txtLogradouro.setError(getString(R.string.error_valor_inválido));
            focusView = txtLogradouro;
            cancel = true;
        }
        if (txtBairro.getText().toString().contentEquals(" ")) {
            txtBairro.setError(getString(R.string.error_valor_inválido));
            focusView = txtBairro;
            cancel = true;
        }
        if (txtCidade.getText().toString().contentEquals(" ")) {
            txtCidade.setError(getString(R.string.error_valor_inválido));
            focusView = txtCidade;
            cancel = true;
        }


        if (cancel) {
            focusView.requestFocus();
        } else {
            cancel = false;
        }
        return cancel;

    }

}
