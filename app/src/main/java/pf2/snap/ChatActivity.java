package pf2.snap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import model.Chat;
import model.Endereco;
import model.ItemServico;
import model.Mensagem;
import utilidades.Sessao;


public class ChatActivity extends Activity {
    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        final EditText txtMensagem = (EditText) findViewById(R.id.txtMensagem);

        final ArrayList<String> lstMensagensToView = new ArrayList<String>();
        final ListView lstViewMensagens = (ListView) findViewById(R.id.lsChat);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lstMensagensToView);
        //Constantes
        queue = Volley.newRequestQueue(this);
        final int idUsuario = Sessao.getInstance().getIdUsuario();
        final Mensagem mensagem = new Mensagem();
        final List<Mensagem> listMsg = new ArrayList<Mensagem>();

        //TROCAR HARD CODE
        final int idChat = 4;
        Intent intent = getIntent();
        final ItemServico item = (ItemServico) intent.getSerializableExtra("item");
        //TROCAR HARD CODE

        if(Sessao.getInstance().getIdCliente() != 0){
            setTitle("Chat com " + item.getDesFornecedor());
        }else {
            setTitle("Chat com " + item.getDesCliente());
        }


        //atribui a mensagem ao usuário que enviou.
        mensagem.setIdUsuario(idUsuario);

        final String url = "http://54.69.232.33:8080/webService_Snap/rest/chat/";

        //if(Fornecedor) exibe nome fornecedor senão exibe nome cliente
        lstViewMensagens.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        Button btnEnviar = (Button) findViewById(R.id.btnEnviar);
        btnEnviar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                lstMensagensToView.add("Você disse: " + txtMensagem.getText().toString() + "\n\n");
                lstViewMensagens.setAdapter(adapter);
                adapter.notifyDataSetChanged();

                mensagem.setTexto(txtMensagem.getText().toString());

                StringRequest jsonObjReq = new StringRequest(Request.Method.POST, url + "novaMsg",
                        new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                Log.d("Post", response.toString());

                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Post", "Error: " + error.getMessage());

                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("idUsuario",String.valueOf(mensagem.getIdUsuario()));
                        params.put("idChat",String.valueOf(idChat));
                        params.put("mensagem",mensagem.getTexto());

                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/x-www-form-urlencoded");
                        return params;
                    }

                };

                // Adding request to request queue
                //AppController.getInstance().addToRequestQueue(jsonObjReq);
                queue.add(jsonObjReq);

                txtMensagem.setText("");
            }
        });

        lstViewMensagens.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View v, final int position, long id) {
                //Alerta para clique sobre endereços no ListView
                AlertDialog.Builder adb = new AlertDialog.Builder(ChatActivity.this);
                adb.setTitle("Mensagem enviada em: ");

                //TODO exibir data da msg.....
                adb.setMessage((CharSequence) listMsg.get(position).getData());

                adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });
                adb.show();
            }
        });
        final JsonArrayRequest getRequest = new JsonArrayRequest(url + "getMsg/" + idChat,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        // display response
                        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Log.d("Response", response.toString());
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);
                                Mensagem msg = new Mensagem(
                                        Integer.valueOf(obj.getString("idUsuario")),
                                        obj.getString("texto"),
                                        fmt.parse(obj.getString("data"))
                                );
                                 listMsg.add(msg);
                                if (listMsg.get(i).getIdUsuario() == idUsuario){
                                    lstMensagensToView.add("Você disse: " + listMsg.get(i).getTexto());
                                }else{
                                    if(Sessao.getInstance().getIdCliente() != 0){
                                        lstMensagensToView.add("Usuário "+ item.getDesFornecedor() +" disse: " + listMsg.get(i).getTexto());
                                    }else {
                                        lstMensagensToView.add("Usuário "+ item.getDesCliente() +" disse: " + listMsg.get(i).getTexto());
                                    }

                                }

                                lstViewMensagens.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("Error.Response", error.getMessage());
                    }
                }
        );
        // add it to the RequestQueue
        queue.add(getRequest);

        final Handler handler = new Handler();
        handler.postDelayed( new Runnable() {

            @Override
            public void run() {
                listMsg.clear();
                lstMensagensToView.clear();
                queue.add(getRequest);

                handler.postDelayed( this, 6000 );

            }
        }, 6000 );

    }
}