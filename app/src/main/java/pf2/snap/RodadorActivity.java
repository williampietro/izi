package pf2.snap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import model.Endereco;
import utilidades.Sessao;


public class RodadorActivity extends Activity {

    int idUsuario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rodador);

        Intent intent = getIntent();
        if(intent.getSerializableExtra("idUsuario") != null) {
            idUsuario = (Integer) intent.getSerializableExtra("idUsuario");
        }
        Button btnChat = (Button) findViewById(R.id.btnChat);
        btnChat.setVisibility(View.GONE);
        btnChat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                chamaChat();
            }
        });

        Button btnMaps = (Button) findViewById(R.id.btnMaps);
        if(Sessao.getInstance().getIdFornecedor() > 0) {
            btnMaps.setVisibility(View.GONE);
        }
        btnMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chamaMaps();
            }
        });

        Button btnServicos = (Button) findViewById(R.id.btnServicos);
        btnServicos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chamaServicos();
            }
        });


        Button btnSolicitaServico = (Button) findViewById(R.id.btnSolicitaServico);
        if(Sessao.getInstance().getIdFornecedor() > 0){
            btnSolicitaServico.setVisibility(View.GONE);
        }
        btnSolicitaServico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chamaSolicitaServico();
            }
        });


        Button btnCriaOrcamento = (Button) findViewById(R.id.btnCriaOrcamento);
        //if(Sessao.getInstance().getIdCliente() > 0) {
            btnCriaOrcamento.setVisibility(View.GONE);
        //}
        btnCriaOrcamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chamaCriaOrcamento();
            }
            });

        Button btnAprovarOrcamento = (Button) findViewById(R.id.btnAprovarOrcamento);
        //if(Sessao.getInstance().getIdCliente() > 0){
            btnAprovarOrcamento.setVisibility(View.GONE);
        //}
        btnAprovarOrcamento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chamaAprovarOrcamento();
            }
        });

        Button btnEnderecos = (Button) findViewById(R.id.btnEnderecos);
        if(Sessao.getInstance().getIdFornecedor() > 0) {
            btnEnderecos.setVisibility(View.GONE);
        }
        btnEnderecos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chamaEnderecos();
            }
        });

        Button btnUsuarios = (Button) findViewById(R.id.btnUsuarios);
        btnUsuarios.setVisibility(View.GONE);
        btnUsuarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chamaUsuarios();
            }
        });

        Button btnAgenda = (Button) findViewById(R.id.btnAgenda);
        btnAgenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chamaAgenda();
            }
        });

        Button btnIniciaServico = (Button) findViewById(R.id.btnIniciaServico);
        btnIniciaServico.setVisibility(View.GONE);
        btnIniciaServico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chamaIniciaServico();
            }
        });


        //Toast.makeText(RodadorActivity.this, String.valueOf(idUsuario) , Toast.LENGTH_LONG).show();
    }

    private void chamaAgenda() {
        startActivity(new Intent(this, ListaAgendaActivity.class));
        //startActivity(new Intent(this, AvaliacaoActivity.class));
    }

    private void chamaIniciaServico() {
        startActivity(new Intent(this, IniciarServicoActivity.class));
    }

    private void chamaChat() {
        startActivity(new Intent(this, ChatActivity.class));
    }

    protected void chamaLogin(){
        startActivity(new Intent(this, LoginActivity.class));
    }
    protected void chamaMaps(){
        startActivity(new Intent(this, MapsActivity.class));
    }
    protected void chamaServicos(){
        startActivity(new Intent(this, ServicosActivity.class));
    }
    protected void chamaSolicitaServico(){

        Intent intent = new Intent(this, ListaEnderecoActivity.class);
        // 3. put person in intent data
        intent.putExtra("idUsuario", idUsuario);
        // 4. start the activity
        startActivity(intent);
    }
    protected void chamaCriaOrcamento(){startActivity(new Intent(this, CriaOrcamentoActivity.class));}
    protected void chamaAprovarOrcamento(){startActivity(new Intent(this, AprovarOrcamentoActivity.class));}
    protected void chamaUsuarios()
    {
        AlertDialog.Builder adb = new AlertDialog.Builder(RodadorActivity.this);
        adb.setTitle("Escolha uma opção:");

        adb.setPositiveButton("Cliente", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Intent intent = new Intent(RodadorActivity.this, CadastroUsuarioActivity.class);

                intent.putExtra("idTipoUsuario", 1);

                startActivity(intent);
            }
        });

        adb.setNegativeButton("Fornecedor", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Intent intent = new Intent(RodadorActivity.this, CadastroUsuarioActivity.class);

                intent.putExtra("idTipoUsuario", 2);

                startActivity(intent);
            }
        });

        adb.show();
    }
    protected void chamaEnderecos(){

        Intent intent = new Intent(this, EnderecoActivity.class);
        // 3. put person in intent data
        intent.putExtra("idUsuario", idUsuario);
        // 4. start the activity
        startActivity(intent);

    }

}
