package pf2.snap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import model.ItemServico;
import model.Orcamento;
import model.Servico;
import utilidades.Mask;
import utilidades.Sessao;

import static pf2.snap.R.id.btnCancelar;
import static pf2.snap.R.id.txtDescServico;


public class IniciarServicoActivity extends Activity
{
    final String urlSer = "http://54.69.232.33:8080/webService_Snap/rest/servicos/";
    RequestQueue queue;
    ItemServico item;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciar_servico);

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Aguarde...");
        pDialog.show();

        queue = Volley.newRequestQueue(this);

        final Servico servico = new Servico();

        final TextView txtTituloServico = (TextView) findViewById(R.id.lblTituloDoServico);

        final TextView txtDescServico = (TextView) findViewById(R.id.txtDescServico);
        final TextView txtValorServico = (TextView) findViewById(R.id.txtValorServico);
        final TextView txtDataInicio = (TextView) findViewById(R.id.txtDataInicioServico);
        final TextView txtDataFim = (TextView) findViewById(R.id.txtDataFimServico);

        Button btnIniciarServico = (Button) findViewById(R.id.btnIniciar);
        Button btnFinalizarServico = (Button) findViewById(R.id.btnFinalizar);
        Button btnCancelarServico = (Button) findViewById(R.id.btnCancelar);

        Intent intent = getIntent();
        item = (ItemServico) intent.getSerializableExtra("item");

        final int idServico = item.getIdServico();

        JsonArrayRequest getRequest = new JsonArrayRequest(urlSer + "carregaServico/" + idServico,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response)
                    {
                        // display response
                        Log.d("Response", response.toString());
                        for (int i = 0; i < response.length(); i++)
                        {
                            try
                            {
                                JSONObject obj = response.getJSONObject(i);

                                servico.setDesTitulo(obj.getString("desTitulo"));
                                servico.setDesServico(obj.getString("desServico"));
                                servico.setNumValor(obj.getString("numValor"));
                                servico.setDataInicio(obj.getString("dataInicio"));
                                servico.setDataFim(obj.getString("dataFim"));

                                txtTituloServico.setText(servico.getDesTitulo());
                                txtDescServico.setText(servico.getDesServico());
                                txtValorServico.setText(servico.getNumValor());
                                txtDataInicio.setText(servico.getDataInicio());
                                txtDataFim.setText(servico.getDataFim());
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.d("Error.Response", error.getMessage());
                    }
                }
        );
        // add it to the RequestQueue
        queue.add(getRequest);

        if(Sessao.getInstance().getIdFornecedor() > 0)
        {
            if(servico.getIdStatusServico() == 4)
            {
                btnIniciarServico.setVisibility(View.GONE);
                btnCancelarServico.setVisibility(View.GONE);
            }
            else if(servico.getIdStatusServico() == 2)
            {
                btnFinalizarServico.setVisibility(View.GONE);
            }
        }

        btnIniciarServico.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alteraStatusServico(idServico, 1);

                Toast.makeText(IniciarServicoActivity.this, "Serviço Iniciado", Toast.LENGTH_LONG).show();
            }
        });

        btnCancelarServico.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alteraStatusServico(idServico, 3);

                Toast.makeText(IniciarServicoActivity.this, "Serviço Cancelado", Toast.LENGTH_LONG).show();
            }
        });

        btnFinalizarServico.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                alteraStatusServico(idServico, 4);
                Toast.makeText(IniciarServicoActivity.this, "Serviço Finalizado", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(IniciarServicoActivity.this,AvaliacaoActivity.class);
                intent.putExtra("item",item);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.principal, menu);
        return true;
    }
    public void doThis(MenuItem itemMenu){
        Toast.makeText(this, "Hello World", Toast.LENGTH_LONG).show();

        JsonArrayRequest getRequest = new JsonArrayRequest(urlSer + "buscarPorId/" + item.getIdServico(),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // display response
                        Log.d("Response", response.toString());
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                item.setDesCliente(obj.getString("desCliente"));
                                item.setDesFornecedor(obj.getString("desFornecedor"));

                                Intent intent = new Intent(IniciarServicoActivity.this, ChatActivity.class);
                                intent.putExtra("item", item);
                                startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.getMessage());
                    }
                }
        );
        // add it to the RequestQueue
        queue.add(getRequest);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem itemMenu) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = itemMenu.getItemId();
        switch (itemMenu.getItemId()) {
            default:
            return super.onOptionsItemSelected(itemMenu);
        }
    }

    public void alteraStatusServico(final int  idServico, final int idStatus)
    {
        queue = Volley.newRequestQueue(this);

        StringRequest jsonObjServico = new StringRequest(Request.Method.POST, urlSer + "alterarStatus",new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                Log.d("Post", response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                VolleyLog.d("Post", "Error: " + error.getMessage());
            }
        }){

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("idServico", String.valueOf(idServico));
                params.put("idStatus", String.valueOf(idStatus));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };
        queue.add(jsonObjServico);
    }
}
