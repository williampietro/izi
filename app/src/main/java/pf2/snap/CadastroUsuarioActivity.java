package pf2.snap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import model.CategoriaServicos;
import model.Cliente;
import model.Fornecedor;
import model.Subcategoria;
import model.Usuario;
import utilidades.Mask;
import utilidades.Validador;


public class CadastroUsuarioActivity extends Activity
{
    Intent intent;
    private int idTipoUsuario;
    final String urlUsr = "http://54.69.232.33:8080/webService_Snap/rest/usuario/";

    RequestQueue queue;

    ArrayList<String> lstCategoriaToView;
    String desCategoria;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_usuario);
        queue = Volley.newRequestQueue(this);
        intent = getIntent();
        // pega objeto transferido da mapa
       idTipoUsuario = (Integer)intent.getSerializableExtra("idTipoUsuario");

    //Recupera as informações digitadas em tela.
        final EditText txtCPF = (EditText) findViewById(R.id.txtCPF);
        final EditText txtCNPJ = (EditText) findViewById(R.id.txtCNPJ);
        final EditText txtNome = (EditText) findViewById(R.id.txtNome);
        final EditText txtDataNascimento = (EditText) findViewById(R.id.txtDatanascimento);
        final EditText txtApelido = (EditText) findViewById(R.id.txtApelido);
        final EditText txtSenha = (EditText) findViewById(R.id.txtSenha);
        final EditText txtConfirmaSenha = (EditText) findViewById(R.id.txtConfirmaSenha);
        final EditText txtEmail = (EditText) findViewById(R.id.txtEmail);
        final EditText txtTelefone = (EditText) findViewById(R.id.txtTelefone);
        final EditText txtCelular = (EditText) findViewById(R.id.txtCelular);

        //Spinner categoria
        lstCategoriaToView = new ArrayList<String>();
        lstCategoriaToView.add("Selecione uma categoria");
        final Spinner spnCategoria = (Spinner) findViewById(R.id.spnCategoria);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,lstCategoriaToView);
        ArrayAdapter<String> spinnerArrayAdapter = arrayAdapter;



        if(idTipoUsuario == 1)
        {
            txtCNPJ.setVisibility(View.GONE);
            spnCategoria.setVisibility(View.GONE);
        }
        else if(idTipoUsuario == 2)
        {
            txtCPF.setVisibility(View.GONE);
            txtDataNascimento.setVisibility(View.GONE);
            //busca as categorias
            JsonArrayRequest getRequest = new JsonArrayRequest("http://54.69.232.33:8080/webService_Snap/rest/categoria/listar",
                    new Response.Listener<JSONArray>()
                    {
                        @Override
                        public void onResponse(JSONArray response) {
                            // display response
                            Log.d("Response", response.toString());
                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    JSONObject cat = response.getJSONObject(i);
                                    ArrayList<Subcategoria> listsubcat = new ArrayList<Subcategoria>();
                                    JSONArray subcat = cat.getJSONArray("subcategorias");
                                    for (int x = 0 ; x <subcat.length(); x++) {
                                        Subcategoria subcategoria = new Subcategoria(Integer.valueOf(subcat.getJSONObject(x).getString("id")),
                                                subcat.getJSONObject(x).getString("descricao"));
                                        listsubcat.add(subcategoria);
                                    }
                                    CategoriaServicos categoria = new CategoriaServicos(Integer.valueOf(cat.getString("id")),
                                            cat.getString("descricao"),
                                            Boolean.valueOf(cat.getString("urgente")),listsubcat);
                                    if(!categoria.getUrgente()) {
                                        lstCategoriaToView.add(categoria.getDescricao());
                                        arrayAdapter.notifyDataSetChanged();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Log.d("Error.Response", error.getMessage());
                        }
                    }
            );
            // add it to the RequestQueue
            queue.add(getRequest);

        }

        //Cria as mascaras para os campos necessários
        txtCPF.addTextChangedListener(Mask.insert("###.###.###-##", txtCPF));
        txtCNPJ.addTextChangedListener(Mask.insert("##.###.###/####-##", txtCNPJ));
        txtDataNascimento.addTextChangedListener(Mask.insert("##/##/####", txtDataNascimento));
        txtTelefone.addTextChangedListener(Mask.insert("(##)####-####", txtTelefone));
        txtCelular.addTextChangedListener(Mask.insert("(##)####-####", txtCelular));

        Button btnSalvar = (Button) findViewById(R.id.btnSalvar);
        Button btnAlterar = (Button) findViewById(R.id.btnAlterar);
        Button btnExcluir = (Button) findViewById(R.id.btnExcluir);

        //Valida se ja existe usuário
        //final Usuario usuario = validaUsuario();

        //if(usuario != null)
        //{
            //Carrega campos
        //    populaCampos(usuario);
        //}

        //Cria o evento de clic do Botão Salvar
        btnSalvar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(!validarCampos())
                {

                    final Usuario usuario = new Usuario(txtCPF.getText().toString(),
                                                        txtCNPJ.getText().toString(),
                                                        txtNome.getText().toString(),
                                                        txtDataNascimento.getText().toString(),
                                                        txtApelido.getText().toString(),
                                                        txtSenha.getText().toString(),
                                                        txtEmail.getText().toString(),
                                                        txtTelefone.getText().toString(),
                                                        txtCelular.getText().toString());
                    try
                    {

                        gravaUsuario(usuario);

                        Toast.makeText(CadastroUsuarioActivity.this, "Usuário Cadastrado", Toast.LENGTH_LONG).show();

                        startActivity(new Intent(CadastroUsuarioActivity.this, LoginActivity.class));

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    txtApelido.setText("");
                    txtCelular.setText("");
                    txtConfirmaSenha.setText("");
                    txtCPF.setText("");
                    txtCNPJ.setText("");
                    txtDataNascimento.setText("");
                    txtEmail.setText("");
                    txtNome.setText("");
                    txtSenha.setText("");
                    txtTelefone.setText("");
                }
            }

        });

        btnAlterar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(!validarCampos())
                {
                    //Usuario usuarioNovo = new Usuario();

                    try
                    {
                        //UsuarioBC.getInstance().update(usuario, usuarioNovo);

                        Toast.makeText(CadastroUsuarioActivity.this, "Usuário Atualizado", Toast.LENGTH_LONG).show();
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        });
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spnCategoria.setAdapter(spinnerArrayAdapter);
        spnCategoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int posicao, long id) {
                //Altera subcategoria
                try {
                    //pega a categoria pela posição
                    desCategoria = lstCategoriaToView.get(posicao - 1);


                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btnExcluir.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    //UsuarioBC.getInstance().delete(usuario);

                    Toast.makeText(CadastroUsuarioActivity.this, "Usuário Excluído", Toast.LENGTH_LONG).show();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                txtApelido.setText("");
                txtCelular.setText("");
                txtConfirmaSenha.setText("");
                txtCPF.setText("");
                txtDataNascimento.setText("");
                txtEmail.setText("");
                txtNome.setText("");
                txtSenha.setText("");
                txtTelefone.setText("");
            }
        });
    }

    private boolean validarCampos()
    {
        View focusView = null;

        //Recupera as informações digitadas em tela.
        final EditText txtCPF = (EditText) findViewById(R.id.txtCPF);
        final EditText txtCNPJ = (EditText) findViewById(R.id.txtCNPJ);
        final EditText txtNome = (EditText) findViewById(R.id.txtNome);
        final EditText txtDataNascimento = (EditText) findViewById(R.id.txtDatanascimento);
        final EditText txtApelido = (EditText) findViewById(R.id.txtApelido);
        final EditText txtSenha = (EditText) findViewById(R.id.txtSenha);
        final EditText txtConfirmaSenha = (EditText) findViewById(R.id.txtConfirmaSenha);
        final EditText txtEmail = (EditText) findViewById(R.id.txtEmail);
        final EditText txtTelefone = (EditText) findViewById(R.id.txtTelefone);
        final EditText txtCelular = (EditText) findViewById(R.id.txtCelular);

        if(idTipoUsuario == 1)
        {
            Validador.validateNotNull(txtCPF, "Campo Obrigatório");
        }
        else
        {
            Validador.validateNotNull(txtCNPJ, "Campo Obrigatório");
        }
        Validador.validateNotNull(txtNome, "Campo Obrigatório");
        Validador.validateNotNull(txtDataNascimento, "Campo Obrigatório");
        Validador.validateNotNull(txtApelido, "Campo Obrigatório");
        Validador.validateNotNull(txtSenha, "Campo Obrigatório");
        Validador.validateNotNull(txtConfirmaSenha, "Campo Obrigatório");
        Validador.validateNotNull(txtEmail, "Campo Obrigatório");
        Validador.validateNotNull(txtTelefone, "Campo Obrigatório");
        Validador.validateNotNull(txtCelular, "Campo Obrigatório");

        //Verifica se o CPF ou CNPJ é valido

        if(idTipoUsuario == 1)
        {
            boolean cpfValido = Validador.validateCPF(txtCPF.getText().toString());

            if(!cpfValido)
            {
                txtCPF.setError("CPF Inválido");
                txtCPF.setFocusable(true);

                return true;
            }
        }

        if(idTipoUsuario == 2)
        {
           return false;
        }

        //Verifica se o E-Mail é valido
        boolean emailValido = Validador.validateEmail(txtEmail.getText().toString());

        if(!emailValido)
        {
            txtEmail.setError("E-Mail Inválido");
            txtEmail.setFocusable(true);

            return true;
        }

        if(txtSenha.getText().toString().length() <6){
            txtSenha.setError("Senha deve ter 6 caracteres");
            txtSenha.setFocusable(true);

            return true;
        }
        if(txtSenha.getText().toString().equals(txtConfirmaSenha.getText().toString())){
            txtConfirmaSenha.setError("Senhas diferentes");
            txtConfirmaSenha.setFocusable(true);

            return true;
        }

        return false;
    }


    /*public Usuario validaUsuario()
    {
        Usuario usuario;

        //usuario = UsuarioBC.getInstance().retrive(1);

        return usuario;
    }*/

    public void populaCampos(Usuario usuario)
    {
        final EditText txtCPF = (EditText) findViewById(R.id.txtCPF);
        final EditText txtNome = (EditText) findViewById(R.id.txtNome);
        final EditText txtDataNascimento = (EditText) findViewById(R.id.txtDatanascimento);
        final EditText txtApelido = (EditText) findViewById(R.id.txtApelido);
        final EditText txtSenha = (EditText) findViewById(R.id.txtSenha);
        final EditText txtConfirmaSenha = (EditText) findViewById(R.id.txtConfirmaSenha);
        final EditText txtEmail = (EditText) findViewById(R.id.txtEmail);
        final EditText txtTelefone = (EditText) findViewById(R.id.txtTelefone);
        final EditText txtCelular = (EditText) findViewById(R.id.txtCelular);

        txtCPF.setText(usuario.getNumCPF().toString());
        txtNome.setText(usuario.getNome().toString());
        txtDataNascimento.setText(usuario.getDtaNascimento().toString());
        txtApelido.setText(usuario.getApelido().toString());
        txtSenha.setText("");
        txtConfirmaSenha.setText("");
        txtEmail.setText(usuario.getEmail().toString());
        txtTelefone.setText(usuario.getTelefone().toString());
        txtCelular.setText(usuario.getCelular().toString());
    }

    public void gravaUsuario(final Usuario usuario)
    {
        queue = Volley.newRequestQueue(this);
        StringRequest jsonObjReqCliente = new StringRequest(Request.Method.POST, urlUsr + "add",new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                Log.d("Post", response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                VolleyLog.d("Post", "Error: " + error.getMessage());
            }
        }){

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("id", String.valueOf(usuario.getId()));
                params.put("cpf", String.valueOf(usuario.getNumCPF()));
                params.put("cnpj", String.valueOf(usuario.getNumCNPJ()));
                params.put("categoria", desCategoria);
                params.put("nome", String.valueOf(usuario.getNome()));
                params.put("dtNascimento", String.valueOf(usuario.getDtaNascimento()));
                params.put("apelido", String.valueOf(usuario.getApelido()));
                params.put("senha", String.valueOf(usuario.getSenha()));
                params.put("email", String.valueOf(usuario.getEmail()));
                params.put("telefone", String.valueOf(usuario.getTelefone()));
                params.put("celular", String.valueOf(usuario.getCelular()));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };
        queue.add(jsonObjReqCliente);
    }



}
