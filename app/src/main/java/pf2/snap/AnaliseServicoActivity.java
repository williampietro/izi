package pf2.snap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import model.Cliente;
import model.Endereco;
import model.ItemServico;
import model.Solicitacao;
import model.Usuario;
import utilidades.Sessao;


public class AnaliseServicoActivity extends Activity
{
    final String urlSol = "http://54.69.232.33:8080/webService_Snap/rest/solicitacao/";
    RequestQueue queue;
    ItemServico item;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analise_servico);

        queue = Volley.newRequestQueue(this);

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Aguarde...");
        pDialog.show();

        final Solicitacao solicitacao = new Solicitacao();

        final TextView txtTitulo = (TextView) findViewById(R.id.lblSolicitacao);
        final TextView txtCliente = (TextView) findViewById(R.id.lblCliente);
        final TextView txtTipoServico = (TextView) findViewById(R.id.lblTipoServico);
        final TextView txtDescSolicitacao = (TextView) findViewById(R.id.lblDescSolicitacao);
        final TextView txtEndereco = (TextView) findViewById(R.id.lblEndereco);

        final Button btnAtender = (Button) findViewById(R.id.btnAtender);
        final Button btnRejeitar = (Button) findViewById(R.id.btnRejeitar);

        Intent intent = getIntent();
        item = (ItemServico) intent.getSerializableExtra("item");

        int idSolicitacao = item.getIdSolicitacao();

        JsonArrayRequest getRequest = new JsonArrayRequest(urlSol + "buscarPorId/" + idSolicitacao,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response)
                    {
                        // display response
                        Log.d("Response", response.toString());
                        for (int i = 0; i < response.length(); i++)
                        {
                            try
                            {
                                JSONObject obj = response.getJSONObject(i);

                                Cliente cliente = new Cliente();

                                JSONObject clienteObj = obj.getJSONObject("cliente");

                                cliente.setNumCPF(clienteObj.getString("numCPF"));
                                cliente.setNumCNPJ(clienteObj.getString("numCNPJ"));
                                cliente.setNome(clienteObj.getString("nome"));
                                cliente.setDtaNascimento(clienteObj.getString("dtaNascimento"));
                                cliente.setApelido(clienteObj.getString("apelido"));
                                cliente.setSenha(clienteObj.getString("senha"));
                                cliente.setEmail(clienteObj.getString("email"));
                                cliente.setTelefone(clienteObj.getString("telefone"));
                                cliente.setCelular(clienteObj.getString("celular"));

                                Endereco endereco = new Endereco();

                                JSONObject enderecoObj = obj.getJSONObject("endereco");

                                endereco.setCep(enderecoObj.getString("cep"));
                                endereco.setLogradouro(enderecoObj.getString("logradouro"));
                                endereco.setNumero(enderecoObj.getString("numero"));
                                endereco.setComplemento(enderecoObj.getString("complemento"));
                                endereco.setBairro(enderecoObj.getString("bairro"));
                                endereco.setCidade(enderecoObj.getString("cidade"));
                                endereco.setUf(enderecoObj.getString("uf"));
                                endereco.setLatitude(enderecoObj.getInt("latitude"));
                                endereco.setLongitude(enderecoObj.getInt("longitude"));

                                solicitacao.setTitulo(obj.getString("titulo"));
                                solicitacao.setIdSubcategoria(obj.getInt("idSubcategoria"));
                                solicitacao.setIdEndereco(obj.getInt("idEndereco"));
                                solicitacao.setIdCategoria(obj.getInt("idCategoria"));
                                solicitacao.setCliente(cliente);
                                solicitacao.setDataHora(obj.getString("dataHora"));
                                solicitacao.setDescricao(obj.getString("descricao"));
                                solicitacao.setEndereco(endereco);
                                solicitacao.setIdStatus(obj.getInt("idStatus"));
                                solicitacao.setId(item.getIdSolicitacao());

                                pDialog.hide();

                                txtTitulo.setText(solicitacao.getTitulo().toString());
                                txtCliente.setText("Cliente: " + solicitacao.getCliente().getNome());
                                txtTipoServico.setText("Tipo Servico: " + solicitacao.getTitulo());
                                txtDescSolicitacao.setText("Descrição: " + solicitacao.getDescricao());
                                txtEndereco.setText("Endereço: "+ solicitacao.getEndereco().toString());

                                item.setDesCliente(solicitacao.getCliente().getNome());

                                if(solicitacao.getIdStatus() == 2){
                                    btnAtender.setVisibility(View.GONE);
                                    btnRejeitar.setVisibility(View.GONE);
                                }

                                onRestart();
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.d("Error.Response", error.getMessage());
                    }
                }
        );
        // add it to the RequestQueue
        queue.add(getRequest);

        if(Sessao.getInstance().getIdCliente() != 0){
            btnAtender.setVisibility(View.GONE);
            btnRejeitar.setText("Cancelar");
        }
        onRestart();

        btnAtender.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //Altera o status da solicitaÃ§Ã£o para finalizado, pois aqui se encerra o fluxo da solicitaÃ§Ã£o e se inicia o orÃ§amento.

                StringRequest jsonObjReqSolicitacao = new StringRequest(Request.Method.POST, urlSol + "alteraStatus",new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("Post", response.toString());

                        //Chama a tela de criaÃ§Ã£o de orÃ§amento passando o objeto SolicitaÃ§Ã£o como parametro.
                        Intent intent = new Intent(AnaliseServicoActivity.this, CriaOrcamentoActivity.class);
                        intent.putExtra("solicitacao", solicitacao);
                        startActivity(intent);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        VolleyLog.d("Post", "Error: " + error.getMessage());
                    }
                }){

                    @Override
                    protected Map<String, String> getParams()
                    {
                        int idStatus = 3;

                        Map<String, String> params = new HashMap<String, String>();

                        params.put("idSolicitacao", String.valueOf(item.getIdSolicitacao()));
                        params.put("idStatus", String.valueOf(idStatus));

                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError
                    {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/x-www-form-urlencoded");
                        return params;
                    }

                };

                queue.add(jsonObjReqSolicitacao);
            }
        });

        btnRejeitar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                StringRequest jsonObjReqSolicitacao = new StringRequest(Request.Method.POST, urlSol + "alteraStatus",new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("Post", response.toString());
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        VolleyLog.d("Post", "Error: " + error.getMessage());
                    }
                }){

                    @Override
                    protected Map<String, String> getParams()
                    {
                        int idStatus = 0;
                        if(Sessao.getInstance().getIdUsuario() != 0 ){
                            idStatus = 2;
                        }else{
                            //Logica insana pra setar a solicitacao como nao mais do cara

                        }

                        Map<String, String> params = new HashMap<String, String>();

                        params.put("idSolicitacao", String.valueOf(item.getIdSolicitacao()));
                        params.put("idStatus", String.valueOf(idStatus));

                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError
                    {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/x-www-form-urlencoded");
                        return params;
                    }

                };

                queue.add(jsonObjReqSolicitacao);
            }
        });
    }

}
