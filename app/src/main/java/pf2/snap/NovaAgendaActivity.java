package pf2.snap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import model.Agenda;
import model.Agendamento;
import utilidades.Mask;
import utilidades.Sessao;


public class NovaAgendaActivity extends Activity {
    boolean cancel = false;
    int idAgendamento = 0;
    ProgressDialog pDialog;
    RequestQueue queue;
    final String url = "http://54.69.232.33:8080/webService_Snap/rest/agenda/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nova_agenda);

        final int idFornecedor = Sessao.getInstance().getIdFornecedor();
        Agendamento agendamento = new Agendamento();
        Intent intent = getIntent();
        agendamento = (Agendamento) intent.getSerializableExtra("agendamento");
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Aguarde...");

        queue = Volley.newRequestQueue(this);

        final EditText txtTituloAgenda = (EditText) findViewById(R.id.txtTituloAgenda);
        final EditText txtDescricaoAgenda = (EditText) findViewById(R.id.txtDescricaoAgenda);
        final EditText txtDataInicioAgenda = (EditText) findViewById(R.id.txtDataInicioAgenda);
        final EditText txtHoraInicioAgenda = (EditText) findViewById(R.id.txtHoraInicioAgenda);
        final EditText txtDataFimAgenda = (EditText) findViewById(R.id.txtDataFimAgenda);
        final EditText txtHoraFimAgenda = (EditText) findViewById(R.id.txtHoraFimAgenda);

        txtDataInicioAgenda.addTextChangedListener(Mask.insert("##/##/####", txtDataInicioAgenda));
        txtDataFimAgenda.addTextChangedListener(Mask.insert("##/##/####", txtDataFimAgenda));

        txtHoraInicioAgenda.addTextChangedListener(Mask.insert("##:##", txtHoraInicioAgenda));
        txtHoraFimAgenda.addTextChangedListener(Mask.insert("##:##", txtHoraFimAgenda));

        if(agendamento != null){
            idAgendamento = agendamento.getId() ;
            String dataInicio, dataFim, horaInicio, horaFim;

            dataInicio = agendamento.getDataInicio().substring(0,10);
            dataFim = agendamento.getDataFim().substring(0,10);

            dataInicio = dataInicio.substring(8,10) + "/" + dataInicio.substring(5,7) + "/" + dataInicio.substring(0,4);
            dataFim = dataFim.substring(8,10) + "/" + dataFim.substring(5,7) + "/" + dataFim.substring(0,4);

            horaInicio = agendamento.getDataInicio().substring(11,16);
            horaFim = agendamento.getDataFim().substring(11,16);

            txtTituloAgenda.setText(agendamento.getTitulo());
            txtDescricaoAgenda.setText(agendamento.getDescricao());
            txtDataInicioAgenda.setText(dataInicio);
            txtHoraInicioAgenda.setText(horaInicio);
            txtDataFimAgenda.setText(dataFim);
            txtHoraFimAgenda.setText(horaFim);
        }

        Button btnSalvar = (Button) findViewById(R.id.btnSalvarAgenda);
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validacao()){

                    Agendamento agend = new Agendamento(idAgendamento,0,0,idFornecedor,3,txtDataInicioAgenda.getText().toString()+ " " + txtHoraInicioAgenda.getText().toString(),txtDataFimAgenda.getText().toString() + " " + txtHoraFimAgenda.getText().toString(),
                            txtTituloAgenda.getText().toString(), txtDescricaoAgenda.getText().toString());
                    if (idAgendamento == 0) {
                        //novo
                        makeRequest(agend,"add");
                    }else{
                        //update
                        makeRequest(agend,"update");
                    }

                }
            }
        });
    }
    public void makeRequest(final Agendamento agend, String post){
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, url + post,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("Post", response.toString());
                        Toast.makeText(NovaAgendaActivity.this, "Agendamento gravado com sucesso!", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(NovaAgendaActivity.this, RodadorActivity.class));
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Post", "Error: " + error.getMessage());
                pDialog.hide();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("id", String.valueOf(agend.getId()));
                params.put("idFornecedor", String.valueOf(agend.getIdFornecedor()));
                params.put("titulo", agend.getTitulo());
                params.put("descricao", agend.getDescricao());
                params.put("dataInicio", agend.getDataInicio());
                params.put("dataFim",agend.getDataFim());
                params.put("idTipo", String.valueOf(agend.getIdTipo()));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };
        queue.add(jsonObjReq);
    }

    protected boolean validacao(){

        cancel = false;
        View focusView = null;
        EditText txtTituloAgenda = (EditText) findViewById(R.id.txtTituloAgenda);
        EditText txtDescricaoAgenda = (EditText) findViewById(R.id.txtDescricaoAgenda);
        EditText txtDataInicioAgenda = (EditText) findViewById(R.id.txtDataInicioAgenda);
        EditText txtHoraInicioAgenda = (EditText) findViewById(R.id.txtHoraInicioAgenda);
        EditText txtDataFimAgenda = (EditText) findViewById(R.id.txtDataFimAgenda);
        EditText txtHoraFimAgenda = (EditText) findViewById(R.id.txtHoraFimAgenda);

        if (TextUtils.isEmpty(txtTituloAgenda.getText().toString())) {
            txtTituloAgenda.setError(getString(R.string.error_field_required));
            focusView = txtTituloAgenda;
            cancel = true;
        }

        if (TextUtils.isEmpty(txtDescricaoAgenda.getText().toString())) {
            txtDescricaoAgenda.setError(getString(R.string.error_field_required));
            focusView = txtDescricaoAgenda;
            cancel = true;
        }

        if (TextUtils.isEmpty(txtDataInicioAgenda.getText().toString())) {
            txtDataInicioAgenda.setError(getString(R.string.error_field_required));
            focusView = txtDataInicioAgenda;
            cancel = true;
        }
        if (TextUtils.isEmpty(txtHoraInicioAgenda.getText().toString())) {
            txtHoraInicioAgenda.setError(getString(R.string.error_field_required));
            focusView = txtHoraInicioAgenda;
            cancel = true;
        }
        if (TextUtils.isEmpty(txtDataFimAgenda.getText().toString())) {
            txtDataFimAgenda.setError(getString(R.string.error_field_required));
            focusView = txtDataFimAgenda;
            cancel = true;
        }
        if (TextUtils.isEmpty(txtHoraFimAgenda.getText().toString())) {
            txtHoraFimAgenda.setError(getString(R.string.error_field_required));
            focusView = txtHoraFimAgenda;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            cancel = false;
        }
        return cancel;
    }

}
