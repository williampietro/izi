package pf2.snap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import model.Endereco;
import model.ItemServico;
import model.Solicitacao;
import utilidades.Sessao;


public class ServicosActivity extends Activity {
    boolean firstExec;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicos);
        firstExec = true;
        final String urlStatus = "http://54.69.232.33:8080/webService_Snap/rest/status/";
        final String urlServicos = "http://54.69.232.33:8080/webService_Snap/rest/servicos/";

        final RequestQueue queue = Volley.newRequestQueue(this);
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Aguarde...");
        pDialog.show();
        final ArrayList<String> lstServicosToView = new ArrayList<String>();
        final ArrayList<ItemServico> lstServicos= new ArrayList<ItemServico>();
        final ArrayList<ItemServico> lstServicosExibidos= new ArrayList<ItemServico>();
        final ListView lstViewServicos = (ListView) findViewById(R.id.listViewServicos);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lstServicosToView);
        String paramUrl;

        if(Sessao.getInstance().getIdCliente() > 0){
            paramUrl = "listar/cliente/" + Sessao.getInstance().getIdUsuario();
        }else {
            paramUrl = "listar/fornecedor/"+ Sessao.getInstance().getIdUsuario();
        }

        JsonArrayRequest getRequestServicos = new JsonArrayRequest(urlServicos + paramUrl ,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        // display response
                        Log.d("Response", response.toString());
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                lstServicosToView.add(obj.getString("tipo") + " - " +
                                        obj.getString("titulo")+ " - " + obj.getString("status") );
                                ItemServico item = new ItemServico(obj.getString("tipo"),obj.getString("titulo"),obj.getString("status"),
                                        obj.getInt("idCategoria"),obj.getInt("idOrcamento"),obj.getInt("idServico"),obj.getInt("idFornecedor"), obj.getInt("idCliente"), obj.getInt(("idSolicitacao")));
                                lstServicos.add(item);
                                lstServicosExibidos.add(item);
                                lstViewServicos.setAdapter(adapter);
                                adapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        pDialog.hide();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("Error.Response", error.getMessage());
                        //pDialog.hide();
                    }
                }
        );
        // add it to the RequestQueue
        queue.add(getRequestServicos);


        //Criação e lógica do spinner de status
        final ArrayList<String> lstStatus = new ArrayList<String>();
        lstStatus.add("Todos");
        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, lstStatus);
        Spinner spnStatusServico = (Spinner) findViewById(R.id.spinnerStatusServico);
        ArrayAdapter<String> spinnerArrayAdapter = spinnerAdapter;
        spnStatusServico.setAdapter(spinnerArrayAdapter);
        spnStatusServico.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int posicao, long id) {
                //popula a listView com status selecionado
                String status = parent.getItemAtPosition(posicao).toString();
                if(!firstExec) {
                    lstServicosToView.clear();
                    lstServicosExibidos.clear();
                }

                for (int i = 0; i < lstServicos.size(); i++) {
                    //retorna exatamente o status do servico.
                    if(lstServicos.get(i).getStatus().equals(status)) {

                        lstServicosToView.add(lstServicos.get(i).getTipo() + " - " + lstServicos.get(i).getTitulo() + " - " +
                                lstServicos.get(i).getStatus());

                        ItemServico item = new ItemServico(lstServicos.get(i).getTipo(), lstServicos.get(i).getTitulo(), lstServicos.get(i).getStatus(),
                                lstServicos.get(i).getIdCategoria(), lstServicos.get(i).getIdOrcamento(), lstServicos.get(i).getIdServico()
                                , lstServicos.get(i).getIdFornecedor(), lstServicos.get(i).getIdCliente(), lstServicos.get(i).getIdSolicitacao());
                        lstServicosExibidos.add(item);
                    }
                }
                if("Todos".equals(status)){
                    //Popula listView com todos status
                    for (int i = 0; i < lstServicos.size(); i++) {

                        lstServicosToView.add(lstServicos.get(i).getTipo() + " - " + lstServicos.get(i).getTitulo() + " - " +
                                lstServicos.get(i).getStatus());

                        ItemServico item = new ItemServico(lstServicos.get(i).getTipo(),lstServicos.get(i).getTitulo(),lstServicos.get(i).getStatus(),
                                lstServicos.get(i).getIdCategoria(),lstServicos.get(i).getIdOrcamento(),lstServicos.get(i).getIdServico()
                                ,lstServicos.get(i).getIdFornecedor(), lstServicos.get(i).getIdCliente(), lstServicos.get(i).getIdSolicitacao());
                        lstServicosExibidos.add(item);
                    }
                }
                //variavel usada pra controle na primeira execucao, lstServicos nao pode levar clear...
                firstExec = false;
                //
                lstViewServicos.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        JsonArrayRequest getRequest = new JsonArrayRequest(urlStatus + "listar",
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        // display response
                        Log.d("Response", response.toString());
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                lstStatus.add(response.get(i).toString());
                                spinnerAdapter.notifyDataSetChanged();
                                //pDialog.hide();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("Error.Response", error.getMessage());
                        //pDialog.hide();
                    }
                }
        );
        // add it to the RequestQueue
        queue.add(getRequest);


        lstViewServicos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View v, final int position, long id) {
                lstServicosExibidos.get(position).toString();

                if(lstServicosExibidos.get(position).getIdOrcamento() == 0
                        && lstServicosExibidos.get(position).getIdServico() == 0)
                {

                    intent = new Intent(ServicosActivity.this, AnaliseServicoActivity.class);
                    intent.putExtra("item", lstServicosExibidos.get(position));

                }else if(lstServicosExibidos.get(position).getIdServico() == 0){
                    intent = new Intent(ServicosActivity.this, AprovarOrcamentoActivity.class);
                    intent.putExtra("item", lstServicosExibidos.get(position));

                }else{
                    intent = new Intent(ServicosActivity.this, IniciarServicoActivity.class);
                    intent.putExtra("item", lstServicosExibidos.get(position));

                }
                startActivity(intent);
            }
        });
    }
}
