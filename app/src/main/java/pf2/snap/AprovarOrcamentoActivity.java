package pf2.snap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import model.Agenda;
import model.Agendamento;
import model.Cliente;
import model.Endereco;
import model.ItemServico;
import model.Orcamento;
import model.Servico;
import model.Solicitacao;
import utilidades.Mask;
import utilidades.Sessao;


public class AprovarOrcamentoActivity extends Activity
{
    boolean cancel = false;
    final String urlOrc = "http://54.69.232.33:8080/webService_Snap/rest/orcamento/";
    final String urlSer = "http://54.69.232.33:8080/webService_Snap/rest/servicos/";
    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aprovar_orcamento);

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Aguarde...");
        pDialog.show();

        queue = Volley.newRequestQueue(this);

        final Orcamento orcamento = new Orcamento();
        final Agendamento agendamento = new Agendamento();
        final Solicitacao solicitacao = new Solicitacao();
        final Endereco endereco = new Endereco();
        final Cliente cliente = new Cliente();

        TextView txtTituloOrcamento = (TextView) findViewById(R.id.lblTituloDoServico);
        final TextView txtTituloSolicitacao = (TextView) findViewById(R.id.lblTituloDoServico);

        final EditText txtDescOrcamento = (EditText) findViewById(R.id.txtDescOrcamento);
        final EditText txtValorOrcamento = (EditText) findViewById(R.id.txtValorOrcamento);
        final EditText txtDataInicio = (EditText) findViewById(R.id.txtDataInicioOrcamento);
        final EditText txtDataFim = (EditText) findViewById(R.id.txtDataFimOrcamento);
        Button btnAprovarOrcamento = (Button) findViewById(R.id.btnAprovar);
        Button btnNegociarOrcamento = (Button) findViewById(R.id.btnNegociar);
        btnNegociarOrcamento.setVisibility(View.GONE);
        Button btnReprovarOrcamento = (Button) findViewById(R.id.btnReprovar);

        //txtTituloOrcamento.setText(solicitacao.cabecalhoServico());
        txtDataInicio.addTextChangedListener(Mask.insert("##/##/####", txtDataInicio));
        txtDataFim.addTextChangedListener(Mask.insert("##/##/####", txtDataFim));

        Intent intent = getIntent();
        final ItemServico item = (ItemServico) intent.getSerializableExtra("item");

        final int idOrcamento = item.getIdOrcamento();

        JsonArrayRequest getRequest = new JsonArrayRequest(urlOrc + "buscarPorId/" + idOrcamento,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response)
                    {
                        // display response
                        Log.d("Response", response.toString());
                        for (int i = 0; i < response.length(); i++)
                        {
                            try
                            {
                                JSONObject obj = response.getJSONObject(i);

                                /*JSONObject clienteObj = obj.getJSONObject("cliente");

                                cliente.setNumCPF(clienteObj.getString("numCPF"));
                                cliente.setNumCNPJ(clienteObj.getString("numCNPJ"));
                                cliente.setNome(clienteObj.getString("nome"));
                                cliente.setDtaNascimento(clienteObj.getString("dtaNascimento"));
                                cliente.setApelido(clienteObj.getString("apelido"));
                                cliente.setSenha(clienteObj.getString("senha"));
                                cliente.setEmail(clienteObj.getString("email"));
                                cliente.setTelefone(clienteObj.getString("telefone"));
                                cliente.setCelular(clienteObj.getString("celular"));

                                JSONObject enderecoObj = obj.getJSONObject("endereco");

                                endereco.setCep(enderecoObj.getString("cep"));
                                endereco.setLogradouro(enderecoObj.getString("logradouro"));
                                endereco.setNumero(enderecoObj.getString("numero"));
                                endereco.setComplemento(enderecoObj.getString("complemento"));
                                endereco.setBairro(enderecoObj.getString("bairro"));
                                endereco.setCidade(enderecoObj.getString("cidade"));
                                endereco.setUf(enderecoObj.getString("uf"));
                                endereco.setLatitude(enderecoObj.getInt("latitude"));
                                endereco.setLongitude(enderecoObj.getInt("longitude"));*/

                                JSONObject solicitacaoObj = obj.getJSONObject("solicitacao");

                                solicitacao.setTitulo(solicitacaoObj.getString("titulo"));
                                solicitacao.setIdSubcategoria(solicitacaoObj.getInt("idSubcategoria"));
                                solicitacao.setIdEndereco(solicitacaoObj.getInt("idEndereco"));
                                solicitacao.setIdCategoria(solicitacaoObj.getInt("idCategoria"));
                                //solicitacao.setCliente(cliente);
                                solicitacao.setDataHora(solicitacaoObj.getString("dataHora"));
                                solicitacao.setDescricao(solicitacaoObj.getString("descricao"));
                                //solicitacao.setEndereco(endereco);
                                solicitacao.setIdStatus(solicitacaoObj.getInt("idStatus"));
                                solicitacao.setId(solicitacaoObj.getInt("idSolicitacao"));

                                JSONObject agendamentoObj = obj.getJSONObject("agendamento");

                                agendamento.setIdTipo(agendamentoObj.getInt("idTipo"));
                                agendamento.setDataInicio(agendamentoObj.getString("dataInicio"));
                                agendamento.setDataFim(agendamentoObj.getString("dataFim"));

                                orcamento.setId(idOrcamento);
                                orcamento.setIdStatus(obj.getInt("idStatus"));
                                orcamento.setVlrTotalServico(obj.getString("vlrTotalServico"));
                                orcamento.setSolicitacao(solicitacao);
                                orcamento.setIdFornecedor(obj.getInt("idFornecedor"));
                                orcamento.setDesOrcamento(obj.getString("desOrcamento"));
                                orcamento.setAgendamento(agendamento);

                                txtTituloSolicitacao.setText(orcamento.getSolicitacao().getTitulo().toString());
                                txtDescOrcamento.setText(orcamento.getDesOrcamento().toString());
                                txtValorOrcamento.setText(orcamento.getVlrTotalServico().toString());

                                txtDataInicio.setText(orcamento.getAgendamento().getDataInicio().toString());
                                txtDataFim.setText(orcamento.getAgendamento().getDataFim().toString());

                                pDialog.hide();
                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.d("Error.Response", error.getMessage());
                    }
                }
        );
        // add it to the RequestQueue
        queue.add(getRequest);

        btnAprovarOrcamento.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                if(!validacao())
                {
                    //Obejetos de apoio para validar se ouve alteração das informações.
                    final Orcamento orcamentoAprovacao = new Orcamento();
                    final Agendamento agendaAprovacao = new Agendamento();

                    agendaAprovacao.setDataInicio(txtDataInicio.getText().toString());
                    agendaAprovacao.setDataFim(txtDataFim.getText().toString());

                    orcamentoAprovacao.setVlrTotalServico(txtValorOrcamento.getText().toString());
                    orcamentoAprovacao.setDesOrcamento(txtDescOrcamento.getText().toString());
                    orcamentoAprovacao.setAgendamento(agendaAprovacao);

                    //Comparação do Objeto Original com o Objeto Atualizado
                    //Se forem igual o status do Orçamento é alterado e so serviço é criado.
                    //Se forem diferentes uma mensage de alerta é apresentada.
                    if(orcamentoAprovacao.getVlrTotalServico().equals(orcamento.getVlrTotalServico()) &&
                       orcamentoAprovacao.getDesOrcamento().equals(orcamento.getDesOrcamento()) &&
                       orcamentoAprovacao.getAgendamento().getDataInicio().equals(orcamento.getAgendamento().getDataInicio()) &&
                       orcamentoAprovacao.getAgendamento().getDataFim().equals(orcamento.getAgendamento().getDataFim()))
                    {
                        try
                        {
                            final Servico servico = new Servico();

                            servico.setAgendamento(agendamento);
                            servico.setDesServico(orcamento.getDesOrcamento());
                            servico.setIdStatusServico(2);
                            servico.setNumAvaliacaoCliente(0);
                            servico.setNumAvaliacaoFornecedor(0);
                            servico.setNumValor(orcamento.getVlrTotalServico());
                            servico.setOrcamento(orcamento);

                            aprovarOrcamento(orcamentoAprovacao);

                            criarServico(servico);
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        Toast.makeText(AprovarOrcamentoActivity.this, "Não é possível aprovar esse orçamento pois houve alteração em suas informações!", Toast.LENGTH_LONG).show();
                    }

                }
            }
        });

        btnNegociarOrcamento.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(!validacao())
                {
                    //Obejetos de apoio para validar se ouve alteração das informações.
                    final Orcamento orcamentoNegociacao = new Orcamento();
                    final Agendamento agendaNegociacao = new Agendamento();

                    agendaNegociacao.setDataInicio(txtDataInicio.getText().toString());
                    agendaNegociacao.setDataFim(txtDataFim.getText().toString());

                    orcamentoNegociacao.setVlrTotalServico(txtValorOrcamento.getText().toString());
                    orcamentoNegociacao.setDesOrcamento(txtDescOrcamento.getText().toString());
                    orcamentoNegociacao.setAgendamento(agendaNegociacao);


                    if(orcamentoNegociacao.getVlrTotalServico().equals(orcamento.getVlrTotalServico()) &&
                            orcamentoNegociacao.getDesOrcamento().equals(orcamento.getDesOrcamento()) &&
                            orcamentoNegociacao.getAgendamento().getDataInicio().equals(orcamento.getAgendamento().getDataInicio()) &&
                            orcamentoNegociacao.getAgendamento().getDataFim().equals(orcamento.getAgendamento().getDataFim()))
                    {
                        Toast.makeText(AprovarOrcamentoActivity.this, "Não é possível enviar este orçamento para negociação pois não houve nenhuma alteração em suas informações !", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        try
                        {
                            negociarOrcamento(orcamentoNegociacao);
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        btnReprovarOrcamento.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    reprovarOrcamento(orcamento);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

    }
    protected boolean validacao()
    {
        cancel = false;
        View focusView = null;

        EditText txtDescOrcamento = (EditText) findViewById(R.id.txtDescOrcamento);
        EditText txtValorOrcamento = (EditText) findViewById(R.id.txtValorOrcamento);
        EditText txtDataInicio = (EditText) findViewById(R.id.txtDataInicioOrcamento);
        EditText txtDataFim = (EditText) findViewById(R.id.txtDataFimOrcamento);

        if (TextUtils.isEmpty(txtDescOrcamento.getText().toString())) {
            txtDescOrcamento.setError(getString(R.string.error_field_required));
            focusView = txtDescOrcamento;
            cancel = true;
        }

        if (TextUtils.isEmpty(txtValorOrcamento.getText().toString())) {
            txtValorOrcamento.setError(getString(R.string.error_field_required));
            focusView = txtValorOrcamento;
            cancel = true;
        }

        if (TextUtils.isEmpty(txtDataInicio.getText().toString())) {
            txtDataInicio.setError(getString(R.string.error_field_required));
            focusView = txtDataInicio;
            cancel = true;
        }
        if (TextUtils.isEmpty(txtDataFim.getText().toString())) {
            txtDataFim.setError(getString(R.string.error_field_required));
            focusView = txtDataFim;
            cancel = true;
        }

        if (txtDescOrcamento.getText().toString().contentEquals(" ")) {
            txtDescOrcamento.setError(getString(R.string.error_valor_inválido));
            focusView = txtDescOrcamento;
            cancel = true;
        }
        //TODO testar se datas não são invertidas.


        if (cancel)
        {
            focusView.requestFocus();
        }
        else
        {
            cancel = false;
        }
        return cancel;
    }

    public void aprovarOrcamento(final Orcamento orcamento)
    {
        queue = Volley.newRequestQueue(this);

        StringRequest jsonObjReqOrcamentoAprov = new StringRequest(Request.Method.POST, urlOrc + "aprovar",new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                Log.d("Post", response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                VolleyLog.d("Post", "Error: " + error.getMessage());
            }
        }){

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("idorcamento", String.valueOf(orcamento.getId()));
                params.put("idStatusOrcamento", String.valueOf(3));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };
        queue.add(jsonObjReqOrcamentoAprov);

        Toast.makeText(AprovarOrcamentoActivity.this, "Orçamento Aprovado", Toast.LENGTH_LONG).show();
    }

    public void negociarOrcamento(final Orcamento orcamento)
    {
        queue = Volley.newRequestQueue(this);

        StringRequest jsonObjReqOrcamentoNeg = new StringRequest(Request.Method.POST, urlOrc + "negociar",new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                Log.d("Post", response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                VolleyLog.d("Post", "Error: " + error.getMessage());
            }
        }){

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("id", String.valueOf(orcamento.getId()));
                params.put("valor", String.valueOf(orcamento.getVlrTotalServico()));
                params.put("descricao", String.valueOf(orcamento.getDesOrcamento()));
                params.put("dataInicio", String.valueOf(orcamento.getAgendamento().getDataInicio()));
                params.put("dataFim", String.valueOf(orcamento.getAgendamento().getDataFim()));
                params.put("idSolicitacao", String.valueOf(orcamento.getSolicitacao().getId()));
                params.put("idFfornecedor", String.valueOf(Sessao.getInstance().getIdFornecedor()));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };
        queue.add(jsonObjReqOrcamentoNeg);

        Toast.makeText(AprovarOrcamentoActivity.this, "Orçamento Enviado para Negociação", Toast.LENGTH_LONG).show();
    }

    public void reprovarOrcamento(final Orcamento orcamento)
    {
        queue = Volley.newRequestQueue(this);

        StringRequest jsonObjReqOrcamentoRep = new StringRequest(Request.Method.POST, urlOrc + "reprovar",new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                Log.d("Post", response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                VolleyLog.d("Post", "Error: " + error.getMessage());
            }
        }){

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("idOrcamento", String.valueOf(orcamento.getId()));
                params.put("idStatusOrcamento", String.valueOf(2));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };
        queue.add(jsonObjReqOrcamentoRep);

        Toast.makeText(AprovarOrcamentoActivity.this, "Orçamento Reprovado", Toast.LENGTH_LONG).show();
    }

    public void criarServico(final Servico servico)
    {
        queue = Volley.newRequestQueue(this);

        StringRequest jsonObjReqServico = new StringRequest(Request.Method.POST, urlSer + "add",new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                Log.d("Post", response.toString());

                Toast.makeText(AprovarOrcamentoActivity.this, "Serviço Criado", Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                VolleyLog.d("Post", "Error: " + error.getMessage());
            }
        }){

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();


                params.put("dataInicio", String.valueOf(servico.getOrcamento().getAgendamento().getDataInicio()));
                params.put("dataFim", String.valueOf(servico.getOrcamento().getAgendamento().getDataFim()));
                params.put("valorServico", String.valueOf(servico.getNumValor()));
                params.put("descServico", String.valueOf(servico.getDesServico()));
                params.put("avaliacaoCli", String.valueOf(servico.getNumAvaliacaoCliente()));
                params.put("avaliacaoFor", String.valueOf(servico.getNumAvaliacaoFornecedor()));
                params.put("idStatusServico", String.valueOf(servico.getIdStatusServico()));
                params.put("idOrcamento", String.valueOf(servico.getOrcamento().getId()));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };
        queue.add(jsonObjReqServico);
    }
}
