package pf2.snap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import model.Endereco;
import model.Usuario;
import utilidades.Sessao;


public class ListaEnderecoActivity extends Activity {

    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_endereco);

        final ArrayList<String> lstEnderecosToView = new ArrayList<String>();
        final ListView lstViewEnderecos = (ListView) findViewById(R.id.lvEnderecos);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lstEnderecosToView);
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Aguarde...");

        final Usuario usuario = new Usuario();
        intent = getIntent();
        // pega objeto transferido da mapa
        //usuario.setId((Integer) intent.getSerializableExtra("idUsuario"));
        usuario.setId(Sessao.getInstance().getIdUsuario());

        final String url = "http://54.69.232.33:8080/webService_Snap/rest/endereco/";
        final RequestQueue queue = Volley.newRequestQueue(this);
        pDialog.show();
        JsonArrayRequest getRequest = new JsonArrayRequest(url + "buscar/" + usuario.getId(),
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        // display response
                        Log.d("Response", response.toString());
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Endereco mEndereco = new Endereco(
                                        obj.getInt("idEndereco"),obj.getString("cep"),obj.getString("logradouro")
                                        ,obj.getString("numero"),obj.getString("complemento"),obj.getString("bairro"),
                                        obj.getString("cidade"),obj.getString("uf"),0,0);
                                usuario.addEndereco(mEndereco);
                                lstEnderecosToView.add(mEndereco.toString());
                                lstViewEnderecos.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                                pDialog.hide();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        if(lstEnderecosToView.isEmpty()){
                            intent = new Intent(ListaEnderecoActivity.this, MapsActivity.class);
                            intent.putExtra("idUsuario", usuario.getId());
                            pDialog.hide();
                            startActivity(intent);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("Error.Response", error.getMessage());
                        pDialog.hide();
                    }
                }
        );
        // add it to the RequestQueue
        queue.add(getRequest);



                lstViewEnderecos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View v, final int position, long id) {
                Intent intent = new Intent(ListaEnderecoActivity.this, SolicitaServicoActivity.class);
                ArrayList array = new ArrayList();
                array.add(usuario.getId());
                //array.add(usuario.getPosition(position));
                array.add(usuario.getPosition(position).getId());
                intent.putExtra("IdUsuarioIdEndereco", array);
                startActivity(intent);
            }
        });

        Button btnOkEndereco = (Button) findViewById(R.id.btnNovoEndereco);
        btnOkEndereco.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                intent = new Intent(ListaEnderecoActivity.this, MapsActivity.class);
                // 3. put person in intent data
                intent.putExtra("idUsuario", usuario.getId());
                // 4. start the activity
                startActivity(intent);
            }
        });

    }

}
