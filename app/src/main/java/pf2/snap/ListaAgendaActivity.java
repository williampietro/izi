package pf2.snap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import model.Agenda;
import model.Agendamento;
import model.CategoriaServicos;
import model.ItemServico;
import model.Subcategoria;
import utilidades.Group;
import utilidades.MyExpandableListAdapter;
import utilidades.Sessao;


public class ListaAgendaActivity extends Activity {

    SparseArray<Group> groups = new SparseArray<Group>();
    final String url = "http://54.69.232.33:8080/webService_Snap/rest/agenda/listar/";
    RequestQueue queue;
    int idFornecedor;
    List<Agenda> listAgenda = new ArrayList<Agenda>();
    ExpandableListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_agenda);


        queue = Volley.newRequestQueue(this);
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Aguarde...");
        pDialog.show();

        idFornecedor = Sessao.getInstance().getIdFornecedor();
        listView = (ExpandableListView) findViewById(R.id.lsAgenda);

        JsonArrayRequest getRequestServicos = new JsonArrayRequest(url + idFornecedor ,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        // display response
                        Log.d("Response", response.toString());
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject agendaJson = response.getJSONObject(i);
                                ArrayList<Agendamento> listAgendamento = new ArrayList<Agendamento>();
                                JSONArray agendam = agendaJson.getJSONArray("listAgendamento");
                                for (int x = 0 ; x < agendam.length(); x++) {
                                    Agendamento agendamento = new Agendamento(agendam.getJSONObject(x).getInt("id"),
                                            agendam.getJSONObject(x).getInt("idOrcamento"),
                                            agendam.getJSONObject(x).getInt("idServico"),agendam.getJSONObject(x).getInt("idFornecedor"),agendam.getJSONObject(x).getInt("idTipo"),
                                            agendam.getJSONObject(x).getString("dataInicio"),agendam.getJSONObject(x).getString("dataFim"),
                                            agendam.getJSONObject(x).getString("titulo"),agendam.getJSONObject(x).getString("descricao"));
                                    listAgendamento.add(agendamento);
                                    //construtor agendamento
                                }
                                //construtor agenda
                                Agenda agenda = new Agenda(agendaJson.getString("data"),listAgendamento);
                                listAgenda.add(agenda);
                                //lstViewServicos.setAdapter(adapter);
                                //adapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        for (int j = 0; j < listAgenda.size(); j++) {
                            Group group = new Group(listAgenda.get(j).getData());
                            for (int i = 0; i < listAgenda.get(j).getListAgendamento().size(); i++) {
                                group.children.add(
                                        listAgenda.get(j).getListAgendamento().get(i).getDataInicio().substring(11,16) + " - " +
                                        listAgenda.get(j).getListAgendamento().get(i).getTitulo() + " - " +
                                        listAgenda.get(j).getListAgendamento().get(i).getDescricao());
                            }
                            groups.append(j, group);
                        }
                        //é aqui dentro que está a chamada do listview.clickItem..
                        MyExpandableListAdapter adapter = new MyExpandableListAdapter(ListaAgendaActivity.this,
                                groups,listAgenda);
                        //era ali dentro que estava a chamada do listview.clickItem..
                        listView.setAdapter(adapter);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("Error.Response", error.getMessage());
                        //pDialog.hide();
                    }
                }

        );
        // add it to the RequestQueue
        queue.add(getRequestServicos);
        pDialog.hide();

        Button btnNovaAgenda = (Button) findViewById(R.id.btnNovaAgenda);
        btnNovaAgenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ListaAgendaActivity.this, NovaAgendaActivity.class));
            }
        });

    }

}
